const jwt = require('jsonwebtoken')

const consts = require('../utils/consts')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')


const adminAuth = function(req, res, next) {

    const host = multipleHandler.getHost(req)


    // must have cookie-parser installed
    const token = req.cookies[host.adminToken]

    if (!token) {
        res.status(consts.UNAUTHORIZED_CODE).send('Unauthorized: No token provided')

    } else {

        jwt.verify(token, host.jwtSecret, function(err, decoded) {
            if (err) {

                res.status(consts.UNAUTHORIZED_CODE).send('Unauthorized: Invalid token')

            } else {

                req.username = decoded.username
                next()
            }
        })
    }
}

const customerAuth = function(req, res, next) {

    const host = multipleHandler.getHost(req)

    let token

    const params = {
        // androidToken: req.body[consts.ANDROID_TOKEN],
        customerWebToken: req.cookies[host.customerToken]
    }

    if (params.androidToken)  // if request was from android app
        token = params.androidToken

    else if (params.customerWebToken)  // if request was from customer web
        token = params.customerWebToken


    if (!token) {
        res.sendStatus(consts.UNAUTHORIZED_CODE)

    } else {

        // NOTE: both sign and verify jsonwebtoken functions are
        // (Asynchronous) If a callback is supplied, the callback is called with the err or the JWT.
        // (Synchronous) Returns the result

        jwt.verify(token, host.jwtSecret, function(err, decoded) {
            if (err) {
                res.sendStatus(consts.UNAUTHORIZED_CODE)

            } else {

                req.phone = decoded.phone
                req.code = decoded.code
                next()
            }
        })
    }
}


module.exports = {adminAuth, customerAuth}
