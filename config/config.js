const axios = require("axios")
const fs = require('fs')
const moment = require('moment-timezone')

const dConverter = require('../tools/dateConverter')


// WebStorm / Windows Terminal : set NODE_ENV=development
// Git Bash / Linux Terminal : export NODE_ENV=development
const isDevelopment = (process.env.NODE_ENV === 'development')

// const localNetworkAddresses = [
//     'http://192.168.1.6:2020',
//     'https://192.168.1.6:2020',
//     'http://192.168.1.5:2020',
//     'https://192.168.1.5:2020',
//     'http://192.168.1.4:2020',
//     'https://192.168.1.4:2020',
//     'http://192.168.1.3:2020',
//     'https://192.168.1.3:2020'
// ]


const hostAddress = (isDevelopment)? '0.0.0.0' : '127.0.0.1'

const CWD = process.cwd() // current working directory, it's entirely dependent on what directory the process was launched from

const databaseUrl = 'mongodb://localhost:27017/restaurant'
const staticsPath = CWD +'/statics/'
const imagesPath = CWD +'/statics/images/'
const logPath = CWD +'/config/'


const IDPAY_API_KEY = 'PUBLIC'
const IDPAY_CREATE_TRANSACTION = 'https://api.idpay.ir/v1.1/payment'
const IDPAY_VERIFY_TRANSACTION = 'https://api.idpay.ir/v1.1/payment/verify'


const NESHAN_API_KEY = 'PUBLIC'
const MAPIR_API_KEY = 'PUBLIC'
const PUSHE_TOKEN = 'PUBLIC'


const KAVE_NEGAR_API_KEY = 'PUBLIC'
const KAVE_NEGAR_LOOKUP_URL = `https://api.kavenegar.com/v1/${KAVE_NEGAR_API_KEY}/verify/lookup.json`
const KAVE_NEGAR_SEND_URL = `https://api.kavenegar.com/v1/${KAVE_NEGAR_API_KEY}/sms/send.json`


const MPAYAMAK_SEND_URL = 'https://rest.payamak-panel.com/api/SendSMS/SendSMS'
const MPAYAMAK_CHECK_URL = 'https://rest.payamak-panel.com/api/SendSMS/GetDeliveries2'


const publicKey = 'BF4D-KmyDcU7oHjSfw3RnphdyQ50MjIuI3ZdRUNasrFQkthZqgwr7I-Z3OuRS2VgyP2Oumx-1PnK_3dsXWDgA9k'
const privateKey = 'PUBLIC'

const vapidDetails = {
    subject: "res:test@test.com",
    publicKey,
    privateKey
}

const webPushOptions = {TTL: 12*60*60} // in seconds


class LogWriteStream {

    // constructor(name, fierce){
    //     this._stream = name
    // }
    static getStream() {
        if (this._stream) {
            // console.log('returned Stream')
            return this._stream

        } else {
            // console.log('created Stream')
            // flag: 'a' Open file for appending. The file is created if it does not exist.
            return this._stream = fs.createWriteStream(logPath  + 'logs.txt', {flags:'a'})
        }
    }
}

let db = {}
let gfs = {}
let upload = {}

let checkMeal = {}
let checkMealTimeout = {}
let updateStaticMapImg = {}
let runTransactionChecker = {}

let adminJwtSecret = 'PUBLIC'
let customerJwtSecret = 'PUBLIC'




function log (message, flagParam = '') {

    let signFlag = `${flagParam? '['+flagParam+']' : '' }`

    // no need to write empty lines to log file
    if (message !== '\n')
        LogWriteStream.getStream().write(
            moment().tz("Asia/Tehran").format('YYYY/MM/DD HH:mm:ss')
            + `  ${message}  ${signFlag}\n`)

    // does not need to close cause default option is AutoClose:true when createdWriteStream
    // stream.end()

    // TODO: dont log in real production mode
    // if (isDevelopment)
        console.log(`${message} ${signFlag}`)
    // console.log('\x1b[33m%s\x1b[0m', 'assdfsdgsdf')  //yellow
}


function ensureDirectoryExistence (path, callback) {

    return new Promise(function(resolve, reject) {
        // this is asynchronously
        fs.mkdir(path, { recursive: true }, (err) => {

            if (err) {
                log(err)
                reject()
            }
            else {
                resolve()
                if (callback) callback(err)
                log(`${path} ensured to exist`)
            }
        })
    })
}

function checkFileExistence (path, callback) {

    // Check if the file exists
    fs.access(path, fs.constants.F_OK, (err) => {
        if (callback) callback(err)
    })
}

async function postRequest(url, params, headers) {

    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

    // XMLHttpRequest from a different domain cannot set cookie values for their own domain,
    // unless withCredentials is set to true before making the request.
    const res = await axios.post(url, params, { headers}).catch(err => log(err))

    return {statusCode: res.status, data: res.data}
}

async function getRequestWithParams(url, params) {

    // url must be complete http address
    // var url = new URL(url)

    // console.log(params)

    Object.keys(params).forEach((key, i) => {

        if (i === 0) url += '?'
        url += key + '=' + params[key]

        // if was not last index
        if (i !== Object.keys(params).length - 1) url += '&'
    })

    console.log(url)

    const res = await axios.get(url).catch(err => log(err))

    return {statusCode: res.status, data: res.data}
}

async function getRequest(url, config) {

    const res = await axios.get(url, config).catch(err => log(err))

    return {statusCode: res.status, data: res.data}
}

// NOTE: url must not have http://
async function postToPhp(url, params) {

    return new Promise((resolve, reject) => {

        const http = require('http')

        const querystring = require("querystring")
        const qs = querystring.stringify(params)
        // console.log(qs)

        let hostname
        let path

        for (let i = 0; i < url.length; i++) {
            if (url.charAt(i) === '/') {
                hostname = url.substr(0, i)
                path = url.substring(i, url.length)
                break
            }
        }

        const options = {
            hostname,
            port: 80,
            path,
            method: 'POST',
            headers:{ 'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': qs.length },
            // timeout: 5000,
        }

        let buffer = ""
        const req = http.request(options, function(res) {
            res.on('data', (chunk) => buffer += chunk )
            res.on('end', () => resolve(buffer) )
        })

        req.on('error', function(err) {
            log(err)
        })

        req.write(qs)
        req.end()
    })
}

function replaceAt(index, replacement) {
    return substr(0, index) + replacement + substr(index + replacement.length)
}

function getFormattedPrice (price) {

    if (!price) return price

    price =  price.toString() // converting to String

    return price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function getFormattedFoods(foods) {

    let  temp = ''

    for (let i = 0; i < foods.length; i++) {

        temp += foods[i].name + ' (' + foods[i].count + ')'

        if (i !== foods.length - 1) // if was not last index
            temp += ', '
    }
    return temp
}

function getMealsFromAdmin(admin) {
    return {
        hasTwoMeals: admin.meals.hasTwoMeals,
        lsh : admin.meals.lunch.startHour,
        lsm : admin.meals.lunch.startMin,
        leh : admin.meals.lunch.endHour,
        lem : admin.meals.lunch.endMin,
        dsh : admin.meals.dinner.startHour,
        dsm : admin.meals.dinner.startMin,
        deh : admin.meals.dinner.endHour,
        dem : admin.meals.dinner.endMin,
    }
}

function isDigit(char) {
    return char == '0' || char == '1' || char == '2' || char == '3' || char == '4' || char == '5' || char == '6' || char == '7' || char == '8' || char == '9'
}

function includesLetter(str) {

    for (let i = 0; i < str.length; i++)
        if (isDigit(str.charAt(i)) === false) return true

    return false
}

function monthToString(month) {
    switch (month) {
        case '01': return 'فروردین'
        case '02': return 'اردیبهشت'
        case '03': return 'خرداد'
        case '04': return 'تیر'
        case '05': return 'مرداد'
        case '06': return 'شهریور'
        case '07': return 'مهر'
        case '08': return 'آبان'
        case '09': return 'آذر'
        case '10': return 'دی'
        case '11': return 'بهمن'
        case '12': return 'اسفند'
    }
}

function monthToNumber(number) {
    switch (number) {
        case 'فروردین': return '01'
        case 'اردیبهشت': return '02'
        case 'خرداد': return '03'
        case 'تیر': return '04'
        case 'مرداد': return '05'
        case 'شهریور': return '06'
        case 'مهر': return '07'
        case 'آبان': return '08'
        case 'آذر': return '09'
        case 'دی': return '10'
        case 'بهمن': return '11'
        case 'اسفند': return '12'
    }
}


function getThousandToman(price) {
    price  = price.toString()
    return price.substr(0, price.length-3)
}

function degreesToRadians(degrees) {
    return degrees * Math.PI / 180
}

function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
    var earthRadiusKm = 6371

    var dLat = degreesToRadians(lat2-lat1)
    var dLon = degreesToRadians(lon2-lon1)

    lat1 = degreesToRadians(lat1)
    lat2 = degreesToRadians(lat2)

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2)
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
    return earthRadiusKm * c
}

const toType = (obj) => {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

// for saving database
function getDateInString() {

    const now = dConverter.getLiveDate()
    let day = now.substring(8, 10)

    if (day.charAt(0) === '0') day = day.charAt(1)

    // direction issue
    return day + ' ' + monthToString(now.substring(5, 7)) + ' ' + now.substring(0, 4)
}

function getEventTextDescription(text, maxLength) {

    if (text.length < maxLength) return text // if text length was valid just return

    // attempting to make a description without cutting a word
    for (let i = maxLength - 10; i < text.length; i++) {

        if (text.charAt(i) === ' ' || text.charAt(i) === '،' || text.charAt(i) === '/' ||   text.charAt(i) === '\''
            || text.charAt(i) === '-' || text.charAt(i) === '_')
            return text.substr(0, i ) + ' ...'
    }
}


module.exports = {
    getDateInString,
    staticsPath,
    isDevelopment,
    db,
    CWD,
    // baseAddress,
    // productionAddress,
    // localNetworkAddresses,

    IDPAY_API_KEY,
    IDPAY_CREATE_TRANSACTION,
    IDPAY_VERIFY_TRANSACTION,
    MPAYAMAK_SEND_URL,
    MPAYAMAK_CHECK_URL,
    MPayamakUsername,
    MPayamakPassword,
    MPayamakNumber,
    // managerPanelAddress,
    gfs,
    upload,
    hostAddress,
    databaseUrl,
    imagesPath,
    checkMeal,
    checkMealTimeout,
    log,
    logPath,
    ensureDirectoryExistence,
    checkFileExistence,
    publicKey,
    privateKey,
    adminJwtSecret,
    customerJwtSecret,
    postRequest,
    getRequest,
    getRequestWithParams,
    replaceAt,
    NESHAN_API_KEY,
    MAPIR_API_KEY,
    updateStaticMapImg,
    getFormattedFoods,
    webPushOptions,
    PUSHE_TOKEN,
    isDigit,
    monthToString,
    getThousandToman,
    includesLetter,
    postToPhp,
    distanceInKmBetweenEarthCoordinates,
    monthToNumber,
    KAVE_NEGAR_API_KEY,
    KAVE_NEGAR_LOOKUP_URL,
    vapidDetails,
    toType,
    getEventTextDescription,
    getFormattedPrice,
    getMealsFromAdmin
}
