const mongoose = require('mongoose')
const fs = require('fs')
const moment = require('moment-timezone')

const consts = require("../utils/consts")
const config = require('./config')

// NOTICE: Keep this ORDER between hosts, SMS API is based in their index
const hosts = [
    {
        base: (config.isDevelopment)? 'http://localhost:3030' : 'https://demo.mmnoori.ir',
        admin: (config.isDevelopment)? 'http://localhost:3006' : 'https://demoadmin.mmnoori.ir',
        name: consts.DEMO,
        jwtSecret: 'PUBLIC',
        adminToken: 'at', // Using different Jwt Secret & Cookie keys because in development localhost origin is common between all programs
        customerToken: 'ct',
        assetsKey: 'assets0', // cookie [key] for statics
        title: 'رستوران دمو',
    },
    {
        base: (config.isDevelopment)? 'http://localhost:3032' : 'https://imocoffee.ir',
        admin: (config.isDevelopment)? 'http://localhost:3008' : 'https://admin.imocoffee.ir',
        name: consts.IMO,
        jwtSecret: 'PUBLIC',
        adminToken: 'aat',
        customerToken: 'cct',
        assetsKey: 'assets1',
        title: 'کافه ایمو',
    },
    {
        base: (config.isDevelopment)? 'http://localhost:3033' : 'https://cafe1990.ir',
        admin: (config.isDevelopment)? 'http://localhost:3009' : 'https://admin.cafe1990.ir',
        name: consts.HEZAR,
        jwtSecret: 'PUBLIC',
        adminToken: 'aaat',
        customerToken: 'ccct',
        assetsKey: 'assets2',
        title: 'کافه 1990',
    },

]

const publicKeys = ['BJPx6-xQnornM0TAgKZW6FccWUfjsmtDv_a0gTM0d5RbdOXpgVYt0gbEF9EFeZY2VUEAv7PEZBkJDLc62B9bjDg',
    '']
const privateKeys = ['PUBLIC',
    '']

const databaseUrlBase = 'mongodb://localhost:27017/'

let databaseUrl = []

let connections = []

let models = []

let mealTimeouts = []

let pages = {}







const {adminSchema} = require('../models/admin')
const {customerSchema} = require('../models/customer')
const {discountCodeSchema} = require('../models/discountCode')
const {eventSchema} = require('../models/event')
const {foodSchema} = require('../models/food')
const {foodLabelSchema} = require('../models/foodLabel')
const {messageSchema} = require('../models/message')
const {orderSchema} = require('../models/order')
const {reportSchema} = require('../models/report')
const {staticsSchema} = require('../models/statics')
const {subscriptionSchema} = require('../models/subscription')
const {transactionSchema} = require('../models/transaction')
const {userSchema} = require('../models/user')
const {validationCodeSchema} = require('../models/validationCode')


const init = async () => {


    for (let i = 0; i < hosts.length; i++) {

        databaseUrl.push(databaseUrlBase + hosts[i].name)

        // config.log('hereeeeee', )

        // Mongoose: `findOneAndUpdate()` and `findOneAndDelete()` without the `useFindAndModify` option set to false are deprecated.
        let conn = await mongoose.createConnection(databaseUrl[i], { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })


        connections.push(conn)

        // connections[i].once('open', () => config.log('Connected to MongoDB') )
        // connections[i].on('error', (err) => config.log(`DB Error: ${err}`) )

        models[i] = []

        models[i][consts.ADMIN] = connections[i].model(consts.ADMIN, adminSchema)
        models[i][consts.CUSTOMER] = connections[i].model(consts.CUSTOMER, customerSchema)
        models[i][consts.DISCOUNT_CODE] = connections[i].model(consts.DISCOUNT_CODE, discountCodeSchema)
        models[i][consts.EVENT] = connections[i].model(consts.EVENT, eventSchema)
        models[i][consts.FOOD] = connections[i].model(consts.FOOD, foodSchema)
        models[i][consts.FOOD_LABEL] = connections[i].model(consts.FOOD_LABEL, foodLabelSchema)
        models[i][consts.MESSAGE] = connections[i].model(consts.MESSAGE, messageSchema)
        models[i][consts.ORDER] = connections[i].model(consts.ORDER, orderSchema)
        models[i][consts.REPORT] = connections[i].model(consts.REPORT, reportSchema)
        models[i][consts.STATICS] = connections[i].model(consts.STATICS, staticsSchema)
        models[i][consts.SUBSCRIPTION] = connections[i].model(consts.SUBSCRIPTION, subscriptionSchema)
        models[i][consts.TRANSACTION] = connections[i].model(consts.TRANSACTION, transactionSchema)
        models[i][consts.USER] = connections[i].model(consts.USER, userSchema)
        models[i][consts.VALIDATION_CODE] = connections[i].model(consts.VALIDATION_CODE, validationCodeSchema)

        // mongoose will not create the collection for the model until any documents are created.
        // Use this method to create the collection explicitly.
        // creating collections with mongoose because when we use MONGORESTORE to use backup database
        // model indexes and validator won't work
        // models[i][consts.VALIDATION_CODE].createCollection().then(function(collection) {
        //     console.log('Collection is created!');
        // })

        // console.log('before ' + i)
        // await models[i][consts.ADMIN].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.CUSTOMER].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.DISCOUNT_CODE].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.EVENT].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.FOOD].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.FOOD_LABEL].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.MESSAGE].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.ORDER].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.REPORT].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.STATICS].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.SUBSCRIPTION].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.TRANSACTION].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.USER].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // await models[i][consts.VALIDATION_CODE].createCollection().then((collection) => {
        //     console.log('Collection is created!')
        // })
        // console.log('after ' + i)
    }

}



function getModel(name, flagParam) {

    for (let i = 0; i < hosts.length; i++)

        if (checkCondition(flagParam, hosts[i])) {

            let temp = []

            if (config.toType(name) === 'array') { // if was array
                for (let j = 0; j < name.length; j++)
                    temp[j] = models[i][name[j]]

                return temp
            }

            return models[i][name]
        }
}

function getStaticsPath(flagParam) { return config.staticsPath + getHostByFlag(flagParam).name + '/' }

function getImagesPath(flagParam) { return config.staticsPath + getHostByFlag(flagParam).name + '/images/' }

function getAssetsPath(flagParam) { return config.staticsPath + getHostByFlag(flagParam).name + '/assets/' }

function getNotificationIconPath(flagParam) {
    const host = getHostByFlag(flagParam)
    return host.base + '/static/img/' + host.name + '.png'
}


function getVapidDetails(flagParam) {

    const index = getIndex(flagParam)
    // return {
    //     subject: 'res:test@test.com',
    //     publicKey: publicKeys[index],
    //     privateKey:  privateKeys[index]
    // }
    return {
        subject: 'res:test@test.com',
        publicKey: publicKeys[0],
        privateKey:  privateKeys[0]
    }
}

// ensure static directories to exist
async function ensureStaticPaths () {

    for (let i = 0; i < hosts.length; i++)
        // using promise in case we need to do other things , after pathCheck
        await new Promise(function(resolve, reject) {

            config.ensureDirectoryExistence(config.staticsPath + hosts[i].name + '/images/', (err) => {
                config.ensureDirectoryExistence(config.staticsPath + hosts[i].name + '/assets/', (err) => {
                    resolve()
                })
            })
        })
}

function getJwtSecret(flagParam) { return getHostByFlag(flagParam).jwtSecret }

function getAssetsKey(req) { return getHost(req).assetsKey }

async function initMealsCheck() {
    for (let i = 0; i < hosts.length; i++)
        await checkMeals(0, null, null, hosts[i].name)
}

function getIndex(flagParam) {
    for (let i = 0; i < hosts.length; i++)
        if (checkCondition(flagParam, hosts[i])) return i
}

// ensure that map image files exist
function checkStaticMaps() {

    for (let i = 0; i < hosts.length; i++) {

        const flagParam = hosts[i].name

        config.checkFileExistence(getAssetsPath(flagParam) + 'map.png', (err) => {

            config.log(`${getAssetsPath(flagParam) + 'map.png'} ${err ? 'does NOT exist' : 'exists'}`, flagParam)

            if (err) updateStaticMapImg(flagParam)
        })
    }
}

async function updateStaticMapImg(flagParam) {

    const Admin = getModel(consts.ADMIN, flagParam)

    const admin = await Admin.findOne()

    // let {statusCode, data} = await config.getRequest( `https://api.neshan.org/v2/static?key=`
    //     +`${config.NESHAN_API_KEY}&type=${'dreamy-gold'}&zoom=${'14'}&center=${admin.location[0]},${admin.location[1]}&width=300&height=300&marker=blue`)
    // console.log(config.getRequest('asdasd', {}))
    // config.log(admin.location)

    const {data, statusCode} = await config.getRequest(`https://map.ir/static?width=400&height=250&zoom_level=15&markers=color%3A${'orange'}%7C${admin.location.lng}%2C${admin.location.lat}`,
        { headers: { 'x-api-key': config.MAPIR_API_KEY }, responseType: "arraybuffer" })


    if (statusCode === consts.SUCCESS_CODE) {

        fs.writeFile(getAssetsPath(flagParam)+ 'map.png' , data, function(err) {

            if (err) return config.log(err)
            config.log("The map image saved!", flagParam)
        })
    }

}

function checkMeals(latency, argAdmin, callBack, flagParam) {

    return new Promise((resolve, reject) => {

        const index = getIndex(flagParam)

        mealTimeouts[index] = setTimeout(async () => {

            const liveTime = moment().tz("Asia/Tehran").format('HH:mm:ss')

            const liveHour = liveTime.substr(0,2)
            const liveMinute = liveTime.substr(3,2)
            const liveSecond = liveTime.substr(6,2)

            let liveDate = `${liveHour}:${liveMinute}:${liveSecond}`

            const Admin = getModel(consts.ADMIN, flagParam)

            // only execute query if no admin object was passed
            let admin
            if (!argAdmin) admin = await Admin.findOne({})
            else admin = argAdmin


            if (admin.acceptOrder === consts.OFF) { // if acceptOrder was permanently off, return and call callback if is given

                (callBack)? callBack(admin): ''
                resolve()
                return
            }


            let startLunch = `${admin.meals.lunch.startHour}:${admin.meals.lunch.startMin}:00`
            let endLunch = `${admin.meals.lunch.endHour}:${admin.meals.lunch.endMin}:00`

            let startDinner = `${admin.meals.dinner.startHour}:${admin.meals.dinner.startMin}:00`
            let endDinner = `${admin.meals.dinner.endHour}:${admin.meals.dinner.endMin}:00`

            const isDinnerEnabled = admin.meals.hasTwoMeals

            let isInMidNightDinnerTime = false
            const isDinnerEndAfterMidNight = admin.meals.dinner.endHour < admin.meals.dinner.startHour

            // تحت این شرط هایی که گذاشتیم ساعات ناهار و شام به هیچ وجه نباید توی هم دیگه وارد بشه

            // scenario that dinner ends after midnight, like 00:30
            if (isDinnerEndAfterMidNight && (startDinner < liveDate || endDinner > liveDate)) isInMidNightDinnerTime = true



            // in the format HH:MM:SS you can do a direct string comparison in JS for fuck's sake
            if (((liveDate >= startLunch) && (liveDate < endLunch)) || // in lunch time

                // hasTwoMeals AND          in normal dinner time                      OR in midNight dinner time
                (isDinnerEnabled && (((liveDate >= startDinner) && (liveDate < endDinner)) || isInMidNightDinnerTime)))
            {

                config.log('should accept order', flagParam)

                if (admin.acceptOrder !== consts.TRUE) { // if was not already TRUE
                    admin.acceptOrder = consts.TRUE
                    config.log('Changed acceptOrder = true', hosts[index].name)
                    await admin.save().catch(err => config.log(err))
                }
            } else {

                config.log('should NOT accept order', flagParam)


                if (admin.acceptOrder !== consts.FALSE) { // if was not already FALSE
                    admin.acceptOrder = consts.FALSE
                    config.log('Changed acceptOrder = false', hosts[index].name)
                    await admin.save().catch(err => config.log(err))
                }
            }


            config.log('\n')
            config.log(liveDate)
            config.log(startLunch)
            config.log(endLunch)
            config.log(`hasTwoMeals : ${isDinnerEnabled}`)
            config.log(startDinner)
            config.log(endDinner)


            liveDate = new Date().setHours(liveHour, liveMinute, liveSecond)

            startLunch = new Date().setHours(admin.meals.lunch.startHour, admin.meals.lunch.startMin, 0)
            endLunch = new Date().setHours(admin.meals.lunch.endHour, admin.meals.lunch.endMin, 0)

            startDinner = new Date().setHours(admin.meals.dinner.startHour, admin.meals.dinner.startMin, 0)
            endDinner = new Date().setHours(admin.meals.dinner.endHour, admin.meals.dinner.endMin, 0)


            let remainingTime



            // if (isInMidNightDinnerTime) {
            if (isDinnerEnabled && isInMidNightDinnerTime) { // if has TwoMeals and in midNightDinner

                // live time is after 24
                if (endDinner > liveDate) remainingTime = endDinner - liveDate

                // live time is before 24
                else if (endDinner < liveDate) remainingTime = endDinner+(1000*60*60*24) - liveDate

                config.log('Remaining time to endMidNightDinner')

            } else if (startLunch > liveDate){
                remainingTime = startLunch - liveDate
                config.log('Remaining time to startLunch')

            } else if (endLunch > liveDate) {
                remainingTime = endLunch - liveDate
                config.log('Remaining time to endLunch')

            } else if (isDinnerEnabled && startDinner > liveDate) {
                remainingTime = startDinner - liveDate
                config.log('Remaining time to startDinner')

            } else if (isDinnerEnabled && endDinner > liveDate) {
                remainingTime = endDinner - liveDate
                config.log('Remaining time to endDinner')

            } else { // live time is greater than any other meal time and we are not in midNightDinner time

                remainingTime = startLunch+(1000*60*60*24) - liveDate
                config.log('Remaining time to tommorow startLunch')
            }

            config.log('In Milliseconds: ' + remainingTime , hosts[index].name)
            config.log('In Minutes: ' + Math.round(remainingTime/(1000*60)) , hosts[index].name)

            // it may happen that live time in milliseconds is exactly one of the meal times
            // so to be able to compare remaining time properly we run function with 2 seconds delay
            checkMeals(remainingTime+2000, null, null, flagParam)


            if (callBack) callBack(admin)
            resolve(admin)


            config.log('\n')

        }, latency)

    })
}

function clearMealsCheck(flagParam) {
    for (let i = 0; i < hosts.length; i++)
        if (checkCondition(flagParam, hosts[i])) clearTimeout(mealTimeouts[i])
}

function getHost(req) {
    for (let i = 0; i < hosts.length; i++)
        if (checkCondition(getFlag(req), hosts[i])) return hosts[i]
}

function getHostByFlag(flagParam) {
    for (let i = 0; i < hosts.length; i++)
        if (checkCondition(flagParam, hosts[i])) return hosts[i]
}

function getFlag(req) {

    // req.headers.host is enough
    // req.headers.name is used ONLY on DEVELOPMENT because we are using custom proxy for admin & next that changes headers.host
    if (req.headers.name) return req.headers.name
    else if (req.headers.host) return req.headers.host
}

// next js serverless build
function render(req, res, page) {
    for (let i = 0; i < hosts.length; i++)
        if (checkCondition(getFlag(req), hosts[i])) {
            // pages[i][page].render(req, res)
            global.pages[i][page].render(req, res)
        }
}


function checkCondition(flagParam, host) {
    return host.base.includes(flagParam) || host.admin.includes(flagParam) || flagParam === host.name
}






module.exports = {
    getModel, hosts, getImagesPath, getStaticsPath, init, ensureStaticPaths, getJwtSecret, getAssetsKey, getNotificationIconPath,
    initMealsCheck, checkMeals, clearMealsCheck, getFlag, getHostByFlag, checkStaticMaps, updateStaticMapImg,
    getHost, pages, render, getAssetsPath, getVapidDetails, getIndex
}
