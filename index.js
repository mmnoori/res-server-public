const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const Grid = require('gridfs-stream')

const dConverter = require('./tools/dateConverter')
const config = require('./config/config')
const multipleHandler = require('./config/multipleHandler')
const consts = require('./utils/consts')



// webpush protocol
const webpush = require("web-push")
// console.log(webpush.generateVAPIDKeys())



const backup = require('./tools/backup')
backup()



// Check DB connection
// config.db.once('open', function () {
//     config.log('Connected to MongoDB');
//     // init stream
//     // let gfs = Grid(config.db, mongoose.mongo);
//     // gfs.collection('photos');
//     //
//     // config.gfs = gfs; // saving global grid-file-stream object
// })
//
// // Check for DB errors
// config.db.on('error', function (err) {
//     config.log(`DB Error: ${err}`);
// })

const server = express()


// NOTE: this configurations affect normal api, express.static has its own options

server.disable('x-powered-by', false)
// server.disable('date', false); // required we can't disable it
// server.disable('connection', false); // required we can't disable it
server.disable('etag', false)



// Body Parser MiddleWare
// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
server.use(bodyParser.json())

// cookie-parser parses cookie from req.headers.cookie and makes it accessible via req.cookies
server.use(cookieParser())



// Express session Middleware
// const session = require('express-session');
// const MongoStore = require('connect-mongo')(session);
//
// const store = new MongoStore({
//     mongooseConnection: config.db,
//     // By default, connect-mongo uses MongoDB's TTL collection feature (2.2+) to have mongod instance automatically remove expired sessions.
//     // if cookie maxAge is defined, connect-mongo will ignore ttl option and use cookie.maxAge for session expiration
//     ttl:  12 * 60 * 60
// })

// server.use(session({
//     secret: 'thisIsSecret',
//     store,
//     cookie : {
//         maxAge:  1 * 24 * 3600 * 1000  // cookie will be removed after 1 days
//     },
//     resave: false, // force saving even if there is no change , this effects expiration time
//     saveUninitialized: false
// }))
// be default cookie maxAge is null so cookies will remain forever unless it is changed or overwritten by server session





if (config.isDevelopment) {

    config.log(`We're on developement :)`)

    // we dont need these configuration on production server, cause client and server will be on same domain
    // Access Control Allow configuration
    // with this configuration both first Option req and next req statuses would be 200 OK :)
    server.use(function(req, res, next) {

        const origin = req.headers.origin

        // need to allow only a specific origin when client sending header withCredentials: true

        // Handling Access-Control-Allow-Origin issue on development for local server
        // const allowedOrigins = [config.baseAddress, ...config.localNetworkAddresses]
        //
        // if (allowedOrigins.indexOf(origin) > -1) { // if origin was in allowed origins
        //     res.setHeader('Access-Control-Allow-Origin', origin)
        // }

        // we just need to send withCredentials header to set cookie (only for development because client and server are on different domains)
        // but we can't set 'Access-Control-Allow-Origin' to wildcard '*' when client is sending withCredentials

        // allow specific origins dynamically
        // let allowValue;
        // if (req.headers.origin) allowValue = req.headers.origin;
        // else if (req.headers.host) allowValue = req.headers.host;

        // res.setHeader('Access-Control-Allow-Origin', allowValue);


        // res.setHeader('Access-Control-Allow-Origin', '*');

        res.header('Access-Control-Allow-Credentials', true)
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
        res.setHeader("Access-Control-Allow-Headers", "Cache-Control, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")

        next()
    })
}


// Comment this if you don't need test samples
const faker = require('./tools/faker')

const dataFaker = async function(host) {

    config.log('Running Fake Admin', host.name)
    await faker.insertFakeAdmin(host)

    // config.log('Running Fake Customers');
    // await faker.insertFakeCustomers();

    // config.log('Running Fake Inviteds');
    // await faker.insertFakeInvitedCustomers();

    // config.log('Running Fake Orders');
    // await faker.insertFakeOrders();
}




const admins = require('./routes/admins')
server.use('/api/admin', admins)

const customers = require('./routes/customers')
server.use('/api/customer', customers)

// routes which do not need authentication
const publicRoutes = require('./routes/public')
server.use('/api/', publicRoutes)

const orderHandler = require('./utils/orderHandler')
const staticsHandler = require('./utils/staticsHandler');

// declaring a function and calling it :)
(async () => {

    await multipleHandler.init()

    const hosts = multipleHandler.hosts


    for (let i = 0; i < hosts.length; i++)
        await dataFaker(hosts[i])

    await multipleHandler.initMealsCheck()

    await multipleHandler.ensureStaticPaths()

    await multipleHandler.checkStaticMaps()

    // for (let i = 0; i < hosts.length; i++)
    //     config.runTransactionChecker(hosts[i].name)

})()




// just for test :)
server.get('/api/info', (req, res) => {

    console.log(req.headers)
    // res.header('Cache-Control', 'max-age=60, public');
    // res.header('expires', 'Mon, 02 Mar 2020 18:28:11 GMT');

    // res.removeHeader('ETag');
    // res.header('ETag', '');

    // res.send(req.headers);
    res.send('dsfsdfsdgerhgrujtwulergp[rnj[pbpuovcdpfiwretmhrwthg')
})



// On DEVELOPMENT
// serving all /statics/{name}/ contents by express.static

// On PRODUCTION
// serving /statics/{name}/images content with NGINX
// other /statics/{name}/ content will be served by express.static


server.use(express.static('statics', {
    etag: false,
    lastModified: true,
    // maxAge: '7d',
    // setHeaders: (res, path) => {
    //     res.setHeader('Expires', 'Mon, 02 Mar 2021 18:28:11 GMT');
    // }
}))



// on production
if (!config.isDevelopment) {

    // server.use('/_next', express.static('client/.next'));
    // server.use('/static', express.static('client/public/static'));

    // server.get('/posts/:name', (req, res) => {
    //     nextApp.render(req, res, '/', { name: req.params.name })
    // })

    // server.get('/offline.svg', async (req, res) => {
    //     res.sendFile('offline.svg', { root: path.join(__dirname, '../statics') });
    // })


    // COMMENTED ALL THESE SERVERLESS
    // server.get('/', staticsHandler.incrementView, async (req, res) => {
    //     multipleHandler.render(req, res, consts.INDEX)
    //
    //     // config.log('Start...')
    //     // render returns a promise (getInitialProps directly affects this promise)
    //     // index.render(req, res)
    //     // config.log('END...')
    // })
    //
    // server.get('/events', (req, res) => {
    //     res.redirect('/events/page/1')
    // })
    //
    // server.get('/events/page/:pageNum', staticsHandler.incrementView, (req, res) => {
    //     multipleHandler.render(req, res, consts.EVENTS)
    // })
    //
    // server.get('/events/:eventUrl', staticsHandler.incrementView, (req, res) => {
    //     multipleHandler.render(req, res, consts.EVENT)
    // })
    //
    // server.get('/services', staticsHandler.incrementView, (req, res) => {
    //     multipleHandler.render(req, res, consts.SERVICES)
    // })
    //
    // server.get('/about', staticsHandler.incrementView, (req, res) => {
    //     multipleHandler.render(req, res, consts.ABOUT)
    // })
    //
    // server.get('/invitation/:id', (req, res) => {
    //     multipleHandler.render(req, res, consts.INVITATION)
    // })
    //
    // server.get('/review/:id', (req, res) => {
    //     multipleHandler.render(req, res, consts.REVIEW)
    // })


    server.get(consts.ROUTE_PAYMENT_CALLBACK,  (req, res) => {
        multipleHandler.render(req, res, consts.PAY_RESULT)
    })

    server.post(consts.ROUTE_PAYMENT_CALLBACK, orderHandler.paymentCallback)
}





const port = 4050

server.listen(port, config.hostAddress, (err) => {
    if (err) throw err
    else config.log(`> Ready on http://localhost:${port}`)
})
