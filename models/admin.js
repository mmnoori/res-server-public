const mongoose = require('mongoose')
const consts = require('../utils/consts')
const dConverter = require('../tools/dateConverter')

const adminSchema = mongoose.Schema({
    username:{
        type: String,
        required: true,
        unique: true,
        dropDups: true,
        trim: true
    },
    password:{
        type: String,
        required: true
    },
    address: {
        type: String,
        default: 'رشت - گلسار - بلوار دیلمان'
    },
    addresses: [
        {
            type: String,
        }
    ],
    phone: {
        type: String,
        default: '013-33568974'
    },
    // we don't need to unregister push subscription or service worker from client side
    // cause as soon as admin logins from a new browser, latest subscription will be used for notifications
    subscription: Object,
    location:{  // restaurant location on map
        lng: {
            type: Number,
            default: 49.565977
        },
        lat: {
            type: Number,
            default: 37.288467
        }
    },
    acceptOrder: {
        type: String,
        default: consts.TRUE,
        // TRUE means we are in meal time and acceptOrder, FALSE means we are out of meal time and don't acceptOrder
        // OFF means we dont acceptOrder in and out of meals time, ON is just a temp value till caculations determine it is TRUE or FALSE
        enum: [consts.TRUE, consts.FALSE, consts.OFF, consts.ON]
    },
    bills: [ // adding new bills when trial mode is ended or bill exported
        {
            id: Number,
            price: Number,
            state: {
                type: String,
                default: 'inProcess',
                enum: ['paid', 'exported', 'inProcess']
            },
            lastPayRespite: {
                type: String
            },
            created: {
                type: String,
                default: () => {
                    return dConverter.getLiveDate()
                }
            }
        }
    ],
    dfo: { // dicount on first order (credit on sign up)
        type: Number,
        default: 0
    },
    invitation: {
        discount:{ // in percent
            type: Number,
            min: 1,
            max: 99,
            default: 20,
        },
        minPrice: { // in toman
            type: Number,
            default: 60000
        },
        credit: { // credit to give to inviter
            type: Number,
            default: 5000
        }
    },
    meals: {
        // breakfast: {
        //     startHour: {
        //         type: String,
        //         default: '08'
        //     },
        //     startMin: {
        //         type: String,
        //         default: '00'
        //     },
        //     endHour: {
        //         type: String,
        //         default: '11'
        //     },
        //     endMin: {
        //         type: String,
        //         default: '30'
        //     },
        // },
        lunch: {
            startHour: {
                type: String,
                default: '12'
            },
            startMin: {
                type: String,
                default: '00'
            },
            endHour: {
                type: String,
                default: '16'
            },
            endMin: {
                type: String,
                default: '30'
            },
        },
        dinner: {
            startHour: {
                type: String,
                default: '18'
            },
            startMin: {
                type: String,
                default: '30'
            },
            endHour: {
                type: String,
                default: '23'
            },
            endMin: {
                type: String,
                default: '30'
            },
        },
        hasTwoMeals: {
            type: Boolean,
            default: false
        },
    },
    courier: {
        "0-1": {
            type: Number,
            default: 1000
        },
        "1-2": {
            type: Number,
            default: 2500
        },
        "2-3": {
            type: Number,
            default: 3500
        },
        "3-u": {
            type: Number,
            default: 5000
        },
    },
    leastPrice: {
        type: Number,
        default: 25000
    },
    maxDistance: {
        type: Number,
        default: 3000
    },
    created: {
        type: Date,
        default: () => {
            return Date.now() // Bullet Proof Movement :)
        }
    },
    // these versions are because we don't want to send any repeated request from client
    // at the same time want to handle several clients and don't want to miss any updates in database
    aolv: { // activeOrdersListVersion
        type: Number,
        default: 0
    },
    olv: { // ordersListVersion
        type: Number,
        default: 0
    },
    clv: { // customersListVersion
        type: Number,
        default: 0
    },
    flv: { // foodListVersion
        type: Number,
        default: 0
    },
    elv: { // eventListVersion
        type: Number,
        default: 0
    },

})

module.exports.adminSchema = adminSchema


// module.exports = mongoose.model(consts.ADMIN, adminSchema)
