const mongoose = require('mongoose')

const consts = require ("../utils/consts")
const dConverter = require('../tools/dateConverter')

const customerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    code:{
        type: String,
        required: true,
        unique: true,
        dropDups: true,
    },
    name: { type: String, trim: true },
    phone: { // defining as String because of 0 start issue
        type: String,
        required: true,
        unique: true,
        dropDups: true,
        trim: true
    },
    birth: { // 1397-03-25
        day: String,
        month: String,
        year: String,
    },
    addresses: {
        type: [ // to prevent using population
            {
                address : {
                    type: String,
                    trim: true
                },
                location: {
                    lng:  Number,
                    lat: Number,
                },
                telePhone: { // home number
                    type: String,
                    trim: true
                },
            }
        ],
        validate: [(value) => {return value.length <= 3}, 'addresses exceeds the limit of 3']
    },
    credit: {
        type: Number,
        default: 0
    },
    inviteds: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: consts.CUSTOMER
        }
    ],
    // cause we need to sort by invitedsCount
    // we can use mongoDB aggregation but its definitely not efficient for processing like 5 thousands of customers
    // and then sorting them
    invitedsCount: {
        type: Number,
        default: 0
    },
    inviter: {
        type: mongoose.Schema.Types.ObjectId,
        ref: consts.CUSTOMER
    },
    orders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: consts.ORDER
        }
    ],
    // number of customer's delievered/successful orders
    // cause we need to sort by ordersCount
    ordersCount: {
        type: Number,
        default: 0
    },
    usedDiscounts:{ // array of discountCode codes
        type: Array,
        default: []
    },
    webSubscription: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subscription'
    },
    androidSubscription: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subscription'
    },
    banned: {
        type: Boolean,
        default: false
    },
    created: {
        type: String,
        default: () => {
            return dConverter.getLiveDate()
        }
    },


})

module.exports.customerSchema = customerSchema

// if you export a model as shown below, the model will be scoped
// to Mongoose's default connection.
// module.exports = mongoose.model(consts.CUSTOMER, customerSchema)
