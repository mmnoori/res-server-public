const mongoose = require('mongoose')
const moment = require('moment-timezone')

const consts = require ("../utils/consts")


const discountCodeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    owner:{ // can be String consts.ALL or array of customer codes or phoneNumbers or mongoIds
        type: mongoose.Mixed,
        required: true
    },
    code:{ // must have one non-digit character
        type: String,
        required: true,
        unique: true,
    },
    discount:{ // in percent
        type: Number,
        min: 1,
        max: 99,
        required: true
    },
    minOrderPrice: { // in toman
        type: Number
    },
    occasion: String,
    expires: String,
    created: {
        type: Date,
        default: () => {
            return moment().tz("Asia/Tehran").format('YYYY-MM-DD HH:mm:ss.SSS')+'Z'
        }
    }
})

module.exports.discountCodeSchema = discountCodeSchema


// module.exports = mongoose.model(consts.DISCOUNT_CODE, discountCodeSchema)

