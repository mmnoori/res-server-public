const mongoose = require('mongoose')
const dConverter = require('../tools/dateConverter')
const config = require('../config/config')

const consts = require ("../utils/consts")


const eventSchema = mongoose.Schema({
    title: {
        type: String,
        unique: true
    },
    uri: String,
    desc: String,
    content: String,
    notification: {
        type: Boolean,
        default: false
    },
    date: {// date in String
        type: String,
        default: config.getDateInString
    },
    created: {
        type: String,
        default: () => {
            return dConverter.getLiveDate()
        }
    },
    imgSrc: String
})

module.exports.eventSchema = eventSchema

// module.exports = mongoose.model(consts.EVENT, eventSchema)

