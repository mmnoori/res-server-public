const mongoose = require('mongoose')

const consts = require ("../utils/consts")


const foodSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true,
        dropDups: true
    },
    des: String, // description

    // to add options like. single/double little/medium/big
    options: [
        {
            name: String,
            price: Number
        }
    ],

    // وعده یک/ وعده دو بذار که مثلا برای کافه یا جایی که ناهار شام نداره
    meal: [
        {
            type: String,
            default: 'همه',
            enum: ['شام', 'ناهار', 'صبحانه', 'همه']
        }
    ],

    label: { //cant use ref cause if label get deleted
        type: String,
        required: true
    },
    available: {
        type: Boolean,
        default: true
    },
    suggested: {
        type: Boolean,
        default: false
    },
    price:{
        type: Number,
        required: true
    },
    discount:{ // in percent
        type: Number,
        min: 0,
        max: 50,
        default: 0
    },
    dPrice: { // saving here to help client
        type: Number,
        default: null
    },
    imgSrc: String
})

module.exports.foodSchema = foodSchema


// module.exports = mongoose.model(consts.FOOD, foodSchema)

