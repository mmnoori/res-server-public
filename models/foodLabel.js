const mongoose = require('mongoose')
const consts = require ("../utils/consts")

const foodLabelSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true
    },
})

module.exports.foodLabelSchema = foodLabelSchema


// module.exports = mongoose.model(consts.FOOD_LABEL, foodLabelSchema)

