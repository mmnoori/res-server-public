const mongoose = require('mongoose')
const dConverter = require('../tools/dateConverter')
const config = require('../config/config')

const consts = require ("../utils/consts")


const messageSchema = mongoose.Schema({
    title: String,
    desc: String,

    // NOTE: Object/Array/mongoose.Mixed/{} are the same
    // can be String consts.ALL or customer code
    owner: {},
    date: { // date in String
        type: String,
        default: config.getDateInString
    },
    created: {
        type: String,
        default: () => {
            return dConverter.getLiveDate()
        }
    },
})

module.exports.messageSchema = messageSchema


// module.exports = mongoose.model(consts.MESSAGE, messageSchema)

