const mongoose = require('mongoose')

const consts = require("../utils/consts")
const dConverter = require('../tools/dateConverter')


const orderSchema = mongoose.Schema({
    code:{
        type: String,
        required: true,
        unique: true,
        dropDups: true,
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        ref: consts.CUSTOMER,
        required: true
    },
    phone: String, // for regex query usage

    transid: String,
    method: {
        type: String,
        default: consts.ACCEPTED,
        enum: [consts.ONLINE, consts.CASH]
    },
    from: {
        type: String,
        enum: [consts.ANDROID, consts.WEB]
    },

    state: {
        type: String,
        default: consts.ACCEPTED,
        enum: [consts.REJECTED, consts.ACCEPTED, consts.SENT, consts.DELIVERED, consts.COMMITTED, consts.TO_BE_PAID]
    },
    //  want to save some needless queries for showing order list by not using Food ref
    // querying from customer collection would cost us enough time
    foods: [
        {   // using this structure because we need to save order list independent from food and price future changes
            name: String,
            count: Number,
            price: Number,
            dPrice: Number,
        }
    ],

    telePhone: String,
    // need to save address and coordinates and distance independent from admin or owner address changes like edit or delete
    address: String,
    location: {
        lng: Number,
        lat: Number
    },
    distance: Number, // must calculate distance on new order, cause restaurant location is dynamic


    // need these when cancelling order
    discountCode: String,
    inviterCode: String,

    price:   Number, // final price -> sum of foods price
    cPrice: Number, // courier price

    discounts: Number, // in toman includes discount on foods and discountCode
    credit: Number, // spent credit for this order includes dfo

    payable:{ // payable price -> sum of foods price + cPrice - discount - credit
        type: Number,
        required: true
    },

    wantAtm:{
        type: Boolean,
        default: false
    },

    review: String,
    score: {
        food: {
            type: Number,
            min: 1,
            max: 5,
        },
        courier: {
            type: Number,
            min: 1,
            max: 5,
        },
        app: {
            type: Number,
            min: 1,
            max: 5,
        },
        comment: String,
        created: String
    },


    created: {
        type: String,
        default: () => {
            return dConverter.getLiveDate()
        }
    }
})

module.exports.orderSchema = orderSchema


// module.exports = mongoose.model(consts.ORDER, orderSchema)
