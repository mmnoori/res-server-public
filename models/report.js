const mongoose = require('mongoose')
const dConverter = require('../tools/dateConverter')

const consts = require ("../utils/consts")

const reportSchema = mongoose.Schema({
    title: String,
    desc: String,

    created: {
        type: String,
        default:  () => {
            return dConverter.getLiveDate()
        }
    }
})

module.exports.reportSchema = reportSchema


// module.exports = mongoose.model(consts.REPORT, reportSchema)

