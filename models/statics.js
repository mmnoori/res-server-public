const mongoose = require('mongoose')

const consts = require ("../utils/consts")


const staticsSchema = mongoose.Schema({

    year: Number,

    // mongoose.Mixed/Object/{}/Array are the same
    metrics: mongoose.Mixed,

    views: {
        type: Number,
        default: 0
    },
    customers: {
        type: Number,
        default: 0
    },
    sale: {
        type: Number,
        default: 0
    },
    orders: {
        type: Number,
        default: 0
    },


    // type Object/mongoose.Mixed are the same
    browser: mongoose.Mixed,
    OS:  mongoose.Mixed,

    notDetected: {
        type: Number,
        default: 0
    }

})

module.exports.staticsSchema = staticsSchema


// module.exports = mongoose.model(consts.STATICS, staticsSchema)
