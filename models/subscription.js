const mongoose = require('mongoose')
const consts = require('../utils/consts')



// we don't need to unregister push subscription or service worker from client side
// cause if subscription does not exists webpush.sendNotification() responds 401 and we delete it
// we put a mongodb expiration
const subscriptionSchema = mongoose.Schema({
    platform:{
        type: String,
        enum: [consts.ANDROID, consts.WEB,]
    },
    // type Object/mongoose.Mixed are the same
    subscription: {
        type: Object,
        unique: true,
    },
    createdAt: { type: Date, expires: '60d', default: Date.now }

})

module.exports.subscriptionSchema = subscriptionSchema


// module.exports = mongoose.model(consts.SUBSCRIPTION, subscriptionSchema)
