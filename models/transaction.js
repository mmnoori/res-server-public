const mongoose = require('mongoose')

const consts = require("../utils/consts")
const dConverter = require('../tools/dateConverter')


const transactionSchema = mongoose.Schema({
    transid: {
        type: String,
        required: true,
        unique: true,
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        ref: consts.CUSTOMER,
    },

    amount: Number,
    state: {
        type: Number,
        default: consts.NOT_VERIFIED,
        enum: [consts.NOT_VERIFIED, consts.FAILED, consts.PAYED]
    },

    created: {
        type: Number,
        default: () => {
            return new Date().getTime()
        }
    }
})

module.exports.transactionSchema = transactionSchema


// module.exports = mongoose.model(consts.TRANSACTION, transactionSchema)

