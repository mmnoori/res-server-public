const mongoose = require('mongoose')


const consts = require ("../utils/consts")

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    ip:{
        type: String,
        required: true,
    },
    banned: {
        type: Boolean,
        default: false
    },
    attempts: { // send code attempts
        type: Number,
        default: 1
    },
    phone: { // defining as String because of 0 start issue
        type: String,
        unique: true,
        dropDups: true,
        trim: true
    },

    // I just realized that be cause my collection was created before without expires option,
    // so when I add it, mongo cannot replace this option.
    // NOTICE: The solution is to drop collection each time need to change expires value,
    // otherwise it sticks to expire option defined on collection very creation

    // does not face any problem with mongod service stop, at worst scenario after 1 minute of restart if would delete all expired documents

    // think this must be exactly createdAt for expiration wo work fine

    // can update createdAt to update expiration time

    createdAt: { type: Date, expires: '6h', default: Date.now }

})

module.exports.userSchema = userSchema


// module.exports = mongoose.model(consts.USER, userSchema)

