const mongoose = require('mongoose')

const consts = require ("../utils/consts")


const validationCodeSchema = mongoose.Schema({
    code:{
        type: Number,
        required: true,
        unique: true,
        dropDups: true
    },
    phone: {
        type: String,
        required: true,
        unique: true,
        dropDups: true
    },
    validated: {
        type: Boolean,
        default: false
    },
    // give user ten minutes to complete sign up or sign in
    createdAt: { type: Date, expires: 10*60, default: Date.now }

})

module.exports.validationCodeSchema = validationCodeSchema


// module.exports = mongoose.model(consts.VALIDATION_CODE, validationCodeSchema)
