const express = require('express')
const fs = require('fs')
const router = express.Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const moment = require('moment-timezone')


const auth = require('../auth/middleware')
const consts = require('../utils/consts')
const config = require('../config/config')
const errHandler = require('../utils/errHandler')
const fileManager = require('../tools/fileManager')
const multipleHandler = require('../config/multipleHandler')


const path = require('path')
const crypto = require('crypto') // crypto is a core node js module

const multer  = require('multer')


// using multer-gridfs-storage for storing files with chunkCount in database
// const storage = require('multer-gridfs-storage')({
//     url: config.databaseUrl,
//     file: (req, file) => {
//
//         return new Promise((resolve, reject) => {
//             crypto.randomBytes(16, (err, buf) => {
//
//                 if (err) return reject(err)
//
//                 const filename = buf.toString('hex') + path.extname(file.originalname)
//
//                 if (file.mimetype === 'image/jpeg') {
//                     let fileInfo = {
//                         filename: filename,
//                         bucketName: 'photos', // this should match collection name
//                         chunkSize: 261120 // default: 261120 bytes
//                     }
//
//                     resolve(fileInfo)
//                 } else {
//                     config.log('not JPEG')
//                 }
//             })
//         })
//     }
// })


// let customStorage = require('../tools/CustomStorage')
// let storage = customStorage({
//     destination: function (req, file, callback) {
//
//         callback(null, process.cwd() +'/statics/images')
//     },
//     filename: function (req, file, callback) {
//
//         let extension
//         if (file.mimetype === 'image/jpeg') extension = '.jpeg'
//
//         callback(null, file.fieldname + '-' + Date.now() + extension)
//     }
// })


const upload = multer({
    // storage: storage,
    storage: multer.memoryStorage(),
    limits: { fileSize: 65536 },
    fileFilter: (req, file, cb) => {
        cb(null, true)
    }
})


// we use these shits in Handlers, so we must define and export them before importing Handlers...
async function updateAdminListVersion(listNamesArray, flagParam) {

    const Admin = multipleHandler.getModel(consts.ADMIN, flagParam)

    let admin = await Admin.findOne({})

    if (listNamesArray.includes(consts.FOOD_LIST)) admin.flv++
    if (listNamesArray.includes(consts.CUSTOMER_LIST)) admin.clv++
    if (listNamesArray.includes(consts.ACTIVE_ORDERS)) admin.aolv++
    if (listNamesArray.includes(consts.ORDER_LIST)) admin.olv++
    if (listNamesArray.includes(consts.EVENT_LIST)) admin.elv++

    await admin.save().catch(err => config.log(err))
    return admin
}

module.exports.updateAdminListVersion = updateAdminListVersion // we use this function in handlers, so must be exported before handlres are imported



const adminHandler = require('../utils/adminHandler')
const foodHandler = require('../utils/foodHandler')
const customerHandler = require('../utils/customerHandler')
const orderHandler = require('../utils/orderHandler')
const discountHandler = require('../utils/discountHandler')
const eventHandler = require('../utils/eventHandler')
const reportHandler = require('../utils/reportHandler')






router.post(consts.ROUTE_AUTHENTICATE, adminHandler.authenticate)

router.post(consts.ROUTE_CHECK_TOKEN, auth.adminAuth, function(req, res) { res.send('OK') })

router.post(consts.ROUTE_CHANGE_PASS, auth.adminAuth, adminHandler.changePassword)
router.post(consts.ROUTE_LOGOUT, adminHandler.logOut)

router.post(consts.ROUTE_REGISTER_PUSH, auth.adminAuth, adminHandler.subscribe)
router.post(consts.ROUTE_GET_STATICS, auth.adminAuth, adminHandler.getStatics)

router.post(consts.ROUTE_GET_REPORTS, auth.adminAuth, reportHandler.getReports)

router.post(consts.ROUTE_MANAGEMENT, auth.adminAuth, adminHandler.getManagementDetails)
router.post(consts.ROUTE_UPDATE_MANAGEMENT, auth.adminAuth, adminHandler.updateManagement)

router.post(consts.ROUTE_PAY_PANEL, auth.adminAuth, adminHandler.createPayment)


router.post(consts.ROUTE_GET_CUSTOMERS, auth.adminAuth, customerHandler.getCustomers)
router.post(consts.ROUTE_GET_CUSTOMER_INFO, auth.adminAuth, customerHandler.getInfo)
router.post(consts.ROUTE_BAN_CUSTOMER, auth.adminAuth, customerHandler.banCustomer)

router.post(consts.ROUTE_NEW_DISCOUNT_CODE, auth.adminAuth, discountHandler.newDiscountCode)

router.post(consts.ROUTE_GET_ORDERS, auth.adminAuth, orderHandler.getOrders)

router.post(consts.ROUTE_GET_COMMENTS, auth.adminAuth, orderHandler.getComments)

// must use upload to handle multipart requests, body-parser does not support multipart bodies
// handling add / edit food in update food
router.post(consts.ROUTE_UPDATE_FOOD, auth.adminAuth, upload.single('img'), foodHandler.updateFood)
router.post(consts.ROUTE_DELETE_FOOD, auth.adminAuth, foodHandler.deleteFood)

router.post(consts.ROUTE_FOOD_GROUP_DISCOUNT, auth.adminAuth, foodHandler.setGroupFoodDiscount)

// router.post(consts.ROUTE_ACCEPT_ORDER, auth.adminAuth, orderHandler.acceptOrder)
router.post(consts.ROUTE_UPDATE_MEALS, auth.adminAuth, foodHandler.updateMeals)

router.post(consts.ROUTE_ADD_FOOD_LABEL, auth.adminAuth, foodHandler.addFoodLabel)
router.post(consts.ROUTE_EDIT_FOOD_LABEL, auth.adminAuth, foodHandler.editFoodLabel)
router.post(consts.ROUTE_DELETE_FOOD_LABEL, auth.adminAuth, foodHandler.deleteFoodLabel)


router.post(consts.ROUTE_HANDLE_ORDER, auth.adminAuth, orderHandler.handleOrder)



// handling add / edit food in update event
router.post(consts.ROUTE_UPDATE_EVENT, auth.adminAuth, upload.single('img'), eventHandler.updateEvent)
router.post(consts.ROUTE_DELETE_EVENT, auth.adminAuth, eventHandler.deleteEvent)







// Handle Failed Transactions
config.runTransactionChecker = () => setInterval(async () => {

    // const transactions = await Transaction.find({state: consts.NOT_VERIFIED})
    //
    // if (transactions.length === 0) return config.log('There are no not verified transactions :)')
    //
    //
    // for (const transaction of transactions) {
    //
    //     const transid = transaction.transid
    //
    //     const data = await config.postToPhp(config.AQAYE_PARDAKHT_VERIFY_TRANSACTION, {
    //         amount: transaction.amount, transid, pin: config.AQAYE_PARDAKHT_PIN
    //     })
    //
    //     if (!data) {
    //         config.log('AP did NOT respond')
    //         continue
    //     }
    //
    //     // if response length is less that 3, it is an error code
    //     if (data < 0) {
    //         config.log('Error from AP: ' + data)
    //         continue
    //     }
    //
    //     const passedSeconds = Math.floor((new Date().getTime() - transaction.created) / 1000)
    //     config.log(transid + ' Passed Seconds : ' + passedSeconds)
    //     config.log(data)
    //
    //
    //     let order = await Order.findOne({transid})
    //
    //
    //     if (data == 1) { // payed
    //
    //         config.log('Transaction is payed : ' + transid)
    //
    //
    //         order.state = consts.COMMITTED
    //         transaction.state = consts.PAYED
    //
    //         await order.save().catch(err => {
    //             if (err) config.log(err)
    //         })
    //
    //         await transaction.save().catch(err => {
    //             if (err) config.log(err)
    //         })
    //
    //     }  else if (data == 0 && passedSeconds  > 10*60){ // NOT PAYED AND seconds passed > 600 = 10min
    //
    //         await orderHandler.handleOrderCancel(order, transid)
    //     }
    // }
}, 60*1000)


module.exports = router
