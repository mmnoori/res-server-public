const express = require('express')
const router = express.Router()

const auth = require('../auth/middleware')
const consts = require('../utils/consts')
const config = require('../config/config')
const errHandler = require('../utils/errHandler')

const adminHandler = require('../utils/adminHandler')
const customerHandler = require('../utils/customerHandler')
const orderHandler = require('../utils/orderHandler')
const foodHandler = require('../utils/foodHandler')
const eventHandler = require('../utils/eventHandler')
const staticsHandler = require('../utils/staticsHandler')




router.post(consts.ROUTE_LIST_VERSIONS, auth.adminAuth, adminHandler.getListVersions)

router.post(consts.ROUTE_GET_FOODS,  foodHandler.getFoods)

router.post(consts.ROUTE_GET_EVENTS, eventHandler.getEvents)
router.post(consts.ROUTE_GET_EVENT, eventHandler.getEvent)

router.post(consts.ROUTE_REGISTER_PUSH, customerHandler.registerPushSubscription)
// router.post(consts.ROUTE_PERMISSION_STATE, customerHandler.permissionStateManager)

router.post(consts.ROUTE_INVITATION_INFO, adminHandler.invitationInfo)


router.post(consts.ROUTE_PAYMENT_CALLBACK, handlePaymentCallback)
router.get(consts.ROUTE_PAYMENT_CALLBACK, handlePaymentCallbackGet)


router.post(consts.ROUTE_IDPAY_CUSTOMER_CALLBACK, handleOrderPaymentCallback)
router.get(consts.ROUTE_IDPAY_CUSTOMER_CALLBACK, handleOrderPaymentCallbackGet)





async function handlePaymentCallback(req, res, next) {

    const {status, id} = req.body

    config.log('PaymentCallback POST')

    if (status === consts.SUCCESS_CODE) {

        // this for stoppping idpay to return money
        const {data, statusCode} = await config.postRequest(config.IDPAY_VERIFY_TRANSACTION, {id, [consts.ORDER_ID] : req.body['order_id']},
            {[consts.X_API_KEY]: config.IDPAY_API_KEY, /*[consts.X_SANDBOX]: 1*/ }
            )

        config.log('sent verifyTransaction Request:')
        config.log(data)


        res.sendStatus(consts.SUCCESS_CODE)
    } else {
        res.sendStatus(consts.SUCCESS_CODE)
    }
}

async function handlePaymentCallbackGet(req, res, next) {

    config.log('PaymentCallback GET')

    // just redirect client to react app
    res.writeHead(302, { 'Location': config.managerPanelAddress + consts.ROUTE_PAYMENT_CALLBACK })
    res.end()
}

async function handleOrderPaymentCallback(req, res) {

    // TODO: query and find order from cache, save it in database, send notification to admin
}

async function handleOrderPaymentCallbackGet(req, res) {
}





module.exports = router
