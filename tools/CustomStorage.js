var fs = require('fs');
var path = require('path');

// For fuck's sake can't even access file buffer in custom Storage must use memoryStorage either modify file after save

function MyCustomStorage (opts) {

    this.getDestination = opts.destination;
    this.getFilename = opts.filename;
}

MyCustomStorage.prototype._handleFile = function _handleFile (req, file, cb) {

    this.getDestination(req, file, (err, destination) => {
        if (err) return cb(err);

        this.getFilename(req, file, (err, filename) => {
            if (err) return cb(err);

            let finalPath = path.join(destination, filename);
            let outStream = fs.createWriteStream(finalPath);

            file.stream.pipe(outStream);


            outStream.on('error', cb);
            outStream.on('finish', function () {
                cb(null, {
                    destination: destination,
                    filename: filename,
                    path: finalPath,
                    size: outStream.bytesWritten
                })
            })
        });

    })
};

MyCustomStorage.prototype._removeFile = function _removeFile (req, file, cb) {

    // The purpose of the delete operator is to completely remove a property from an object,
    // whereas setting a property to undefined just sets the property to undefined.
    delete file.destination;
    delete file.filename;
    delete file.path;

    fs.unlink(file.path, cb)
};

module.exports = function (opts) {
    return new MyCustomStorage(opts)
};