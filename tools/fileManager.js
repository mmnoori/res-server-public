const fs = require('fs')
const Jimp = require('jimp')

const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')


function remove(path) {
    fs.unlink(path, (err) => { // synchronous
        if (err) config.log(err)
    })
}

function saveImg(buffer, imgName, flagParam) {

    Jimp.read(buffer)
        .then(image => {

            image.quality(40).cover(246, 184).write(multipleHandler.getImagesPath(flagParam) + imgName)
            console.log('Image Saved')
        })
        .catch(err => {
            config.log(err)
        })

    // fs.writeFile(getImagesPath() + imgName , buffer, function(err) {
    //
    //     if(err) return console.log(err)
    //     console.log("The file was saved!")
    // })
}

function saveEventImg(buffer, imgName, flagParam) {

    Jimp.read(buffer)
        .then(image => {

            image.quality(40).cover(500, 375).write(multipleHandler.getImagesPath(flagParam) + imgName)
            console.log('Image Saved')
        })
        .catch(err => {
            config.log(err)
        })

}

function setFileName(fieldname, mimetype) {
    let extension
    if (mimetype === 'image/jpeg') extension = '.jpeg'

    return fieldname + '-' + Date.now() + extension
}

function getImagesPath() {
    return config.imagesPath
}

module.exports = {remove, saveImg, saveEventImg, setFileName, getImagesPath}
