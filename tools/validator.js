const consts = require('../utils/consts');
const config = require('../config/config');

function acceptOrder(params, res) {

    if (params.acceptOrder !== consts.OFF && params.acceptOrder !== consts.ON)
        return res.sendStatus(consts.BAD_REQ_CODE);

    return false;
}

function sendCode(params, res) {

    if (params.phone.substr(0, 2) !== '09' || params.phone.length !== 11)
        return res.status(consts.BAD_REQ_CODE).send('شماره همراه معتبر نیست');

    return false;
}

function newCustomer(params, res) {


    return false;
}


module.exports = {acceptOrder, sendCode, newCustomer};

