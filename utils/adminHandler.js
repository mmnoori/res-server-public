const mongoose = require('mongoose')
const webpush = require("web-push")
const jwt = require('jsonwebtoken')

const consts = require('./consts')
const errHandler = require('./errHandler')
const notificationHandler = require('./notificationHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const dConverter = require('../tools/dateConverter')



function authenticate (req, res) {

    const Admin = multipleHandler.getModel(consts.ADMIN, multipleHandler.getFlag(req))

    const { username, password } = req.body

    Admin.findOne({ username }, (err, admin) => {

        if (err) errHandler(err)
        else if (!admin) res.status(consts.UNAUTHORIZED_CODE).json({ error: consts.INCORRECT_USER })
        else {

            const host = multipleHandler.getHost(req)

            // NOT HASHING PASSWORDS YET
            // Match password
            // bcrypt.compare(password, user.password, (err, isMatch) => {
            //
            //     if (err) throw err
            //     if (isMatch) {
            //         // Issue token
            //         const payload = { username: username }
            //         const token = jwt.sign(payload, config.adminJwtSecret, { expiresIn: '8h' })
            //
            //         try {
            //             res.cookie(consts.ADMIN_TOKEN, token, { httpOnly: true }).sendStatus(200)
            //         } catch (e) {
            //             config.log(e)
            //         }
            //     } else {
            //         res.status(consts.UNAUTHORIZED_CODE).json({ error: consts.INCORRECT_PASS })
            //     }
            // })

            if (password === admin.password) {

                // Issue token
                const payload = { username }
                const token = jwt.sign(payload, host.jwtSecret, { expiresIn: '8h' })

                try {
                    res.cookie(host.adminToken, token, { httpOnly: true }).sendStatus(consts.SUCCESS_CODE)
                } catch (e) {
                    config.log(e)
                }

            } else {
                res.status(consts.UNAUTHORIZED_CODE).json({ error: consts.INCORRECT_PASS })
            }
        }
    })
}

async function getListVersions(req, res, flagParam) {

    // flagParam is next function by default
    if (config.toType(flagParam) === 'function') flagParam = multipleHandler.getFlag(req)
    const [Admin] = multipleHandler.getModel([consts.ADMIN], flagParam)


    const admin = await Admin.findOne({})
    let response = {
        aolv: admin.aolv, olv: admin.olv,
        flv: admin.flv, clv: admin.clm
    }

    if (req)
        res.json(response)
    else // if this function is called manually with no arguments
        return response
}

async function getManagementDetails(req, res, argAdmin) {

    const [Admin] = multipleHandler.getModel([consts.ADMIN], multipleHandler.getFlag(req))


    let admin  // when we don't call getFoods manually with argAdmin, it is called by router and argAdmin is actually next
    if (argAdmin._id) admin = argAdmin
    else admin = await Admin.findOne({})

    let response = {
        location: admin.location,
        address: admin.address,
        phone: admin.phone,
        courier: admin.courier,
        leastPrice: admin.leastPrice,
        maxDistance: admin.maxDistance,
        dfo: admin.dfo,
        bills: admin.bills,
        acceptOrder: admin.acceptOrder,
        invitation: admin.invitation
    }
    res.json(response)
}

async function updateManagement(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [Admin] = multipleHandler.getModel([consts.ADMIN], flagParam)


    let issue = false

    let {courier, leastPrice, location, maxDistance, dfo, acceptOrder, phone, invitation} = req.body

    let admin = await Admin.findOne({})

    if (courier) {
        admin.courier['0-1'] = courier['0-1']
        admin.courier['1-2'] = courier['1-2']
        admin.courier['2-3'] = courier['2-3']
        admin.courier['3-u'] = courier['3-u']
    }
    if (location && location.lng) { // avoid location = [null]

        admin.location.lng = location.lng
        admin.location.lat = location.lat
    }
    if (location && location.address) admin.address = location.address
    if (maxDistance) admin.maxDistance = maxDistance
    if (leastPrice) admin.leastPrice = leastPrice
    if (dfo) admin.dfo = dfo
    if (invitation) admin.invitation = invitation
    if (acceptOrder) {

        issue = validator.acceptOrder(req.body, res)
        if (issue) return // request was not from react client

        multipleHandler.clearMealsCheck(flagParam)

        if (acceptOrder === consts.ON) admin.acceptOrder = consts.ON  // just a temp value, untill multipleHandler.checkMeals sets the right value
        else  admin.acceptOrder = consts.OFF


        await multipleHandler.checkMeals(0, admin, (fAdmin) => {

            admin = fAdmin
        },  flagParam)

    }
    if (phone) admin.phone = phone

    if (acceptOrder !== consts.ON) // if acceptOrder was changed and it was ON, admin will be saved in checkMeals() function
        await admin.save().catch(err => config.log(err))


    // update static images if location was changed
    if (location) multipleHandler.updateStaticMapImg(flagParam)

    await getManagementDetails(req, res, admin)
}

async function subscribe(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)

    const host = multipleHandler.getHost(req)

    const [Admin] = multipleHandler.getModel([consts.ADMIN], flagParam)


    let issue = false
    let sendPush = false

    let {subscription} = req.body

    let admin = await Admin.findOne({})

    if (JSON.stringify(admin.subscription) !== JSON.stringify(subscription)) sendPush = true // send push if subscription is changed

    admin.subscription = subscription

    await admin.save().catch(err => config.log(err))

    res.sendStatus(consts.SUCCESS_CODE)


    if (sendPush)
        notificationHandler.sendToAdmin(admin.subscription, {
            title: 'پنل ' + host.name,
            content: 'اعلانات و ثبت سفارش های جدید به شما اطلاع داده میشود',
            requireInteraction: false,
            url: multipleHandler.getHost(req).admin
        }, flagParam)

}

async function changePassword(req, res) {

    const [Admin] = multipleHandler.getModel([consts.ADMIN], multipleHandler.getFlag(req))


    let issue = false

    const {pass} = req.body

    await Admin.updateOne({}, {password: pass}).catch(err => {
        errHandler(err, res)
        issue = true
    })

    if (issue) return

    res.sendStatus(consts.SUCCESS_CODE)
}


/*
* this function is for creating bills manually
* */
async function createBill(req, res, next) {


}

async function createPayment(req, res, next) {

    const [Admin] = multipleHandler.getModel([consts.ADMIN], multipleHandler.getFlag(req))


    let issue = false

    const admin = await Admin.findOne({})


    const {statusCode, data} = await config.postRequest(config.IDPAY_CREATE_TRANSACTION,
        {[consts.ORDER_ID]: '4567971239', amount: 20000, callback: config.managerPanelAddress+'/api'+consts.ROUTE_PAYMENT_CALLBACK},
        {
                [consts.X_API_KEY]: config.IDPAY_API_KEY,
                 // [consts.X_SANDBOX]: 1
            }
        )

    config.log(data)

    if (statusCode === consts.CREATED_CODE) {



        res.json(data.link) /* NOTE: if we use res.send() in client response would bo undefined when we parse after using fetch() api */

    } else {
        config.log('Error from IDPAY:')
        config.log(data)

        res.sendStatus(consts.INT_ERR_CODE)
    }

    await admin.save().catch()
}

async function invitationInfo(req, res, next) {

    const [Admin, Customer] = multipleHandler.getModel([consts.ADMIN, consts.CUSTOMER], multipleHandler.getFlag(req))


    let issue = false

    let response = {}

    const {_id} = req.body

    if (_id) {
        const customer = await Customer.findOne({_id})

        response.name = customer.name
        response.code = customer.code

        if (!customer) return res.status(consts.NOT_FOUND_CODE).send('NOT FOUND')
    }

    const admin = await Admin.findOne({})

    res.json({
        ...response,
        discount: admin.invitation.discount,
        minPrice: admin.invitation.minPrice,
        credit: admin.invitation.credit,
    })
}

// async function publicInfo(req, res, next) {
//
//     let response = {
//         address: '',
//         phone: '',
//         meals: {},
//         acceptOrder: {},
//         invitation: {
//             discount: 0,
//             minPrice: 0,
//         }
//     }
//
//     const admin = await Admin.findOne({})
//
//     response.meals = {
//         lsh : admin.meals.lunch.startHour,
//         lsm : admin.meals.lunch.startMin,
//         leh : admin.meals.lunch.endHour,
//         lem : admin.meals.lunch.endMin,
//         dsh : admin.meals.dinner.startHour,
//         dsm : admin.meals.dinner.startMin,
//         deh : admin.meals.dinner.endHour,
//         dem : admin.meals.dinner.endMin,
//     }
//
//     response.acceptOrder = admin.acceptOrder
//     response.address = admin.address
//     response.location = admin.location
//     response.phone = admin.phone
//
//     response.invitation.discount = admin.invitation.discount
//     response.invitation.minPrice = admin.invitation.minPrice
//
//     res.json(response)
// }

async function getStatics(req, res, next) {

    const [Statics] = multipleHandler.getModel([consts.STATICS], multipleHandler.getFlag(req))

    let issue = false

    let {month, year} = req.body

    let response = {
        labels: [],
        views: [],
        orders: [],
        sale: [],
        customers: [],

        sum: {
            views: 0,
            orders: 0,
            sale: 0,
            customers: 0,
        }
    }

    if (!year) { // overall statics

        const statics = await Statics.find({})

        for (let i = 0; i < statics.length; i++) {

            response.labels.push( statics[i].year )

            response.views.push( statics[i]['views'] )
            response.orders.push( statics[i]['orders'] )
            response.sale.push( statics[i]['sale'] )
            response.customers.push( statics[i]['customers'] )

            response.sum.views += statics[i]['views']
            response.sum.orders += statics[i]['orders']
            response.sum.sale += statics[i]['sale']
            response.sum.customers += statics[i]['sale']
        }

    } else {

        if (year) year = Number(year)
        if (month) month = Number(config.monthToNumber(month))

        config.log(year)
        config.log(month)

        const static = await Statics.findOne({year})

        if (!static) return res.json(response)

        if (year && !month) { // month statics

            for (let i = 1; i <= 12; i++) {

                response.labels.push(consts.MONTHS[i-1])

                config.log(static['metrics'][i])

                // if month existed
                if (static['metrics'][i]) {
                    response.views.push( static['metrics'][i]['views'] )
                    response.orders.push( static['metrics'][i]['orders'] )
                    response.sale.push( static['metrics'][i]['sale'] )
                    response.customers.push( static['metrics'][i]['customers'] )
                } else {

                    response.views.push(0)
                    response.orders.push(0)
                    response.sale.push(0)
                    response.customers.push(0)
                }

                // sum
                response.sum.views = static.views
                response.sum.orders = static.orders
                response.sum.customers = static.customers
                response.sum.sale = static.sale
            }

        } else if (year && month) { // day statics

            const days = dConverter.getDaysInJalaliMonth(year, month)
            config.log('days : ' + days)

            for (let i = 1; i <= days; i++) {

                // adding days
                response.labels.push(i)

                // if month existed
                if (static.metrics[month]) {

                    // if day statics existed
                    if (static.metrics[month][i]) {

                        response.views.push( static['metrics'][month][i]['views'] )
                        response.orders.push( static['metrics'][month][i]['orders'] )
                        response.sale.push( static['metrics'][month][i]['sale'] )
                        response.customers.push( static['metrics'][month][i]['customers'] )

                    } else {

                        response.views.push(0)
                        response.orders.push(0)
                        response.sale.push(0)
                        response.customers.push(0)
                    }

                    // sum
                    response.sum.views = static.metrics[month].views
                    response.sum.orders = static.metrics[month].orders
                    response.sum.customers = static.metrics[month].customers
                    response.sum.sale = static.metrics[month].sale

                } else {

                    response.views.push(0)
                    response.orders.push(0)
                    response.sale.push(0)
                    response.customers.push(0)
                }

            }

        }

    }

    res.json(response)
}

function logOut(req, res) {

    try {
        res.cookie(multipleHandler.getHost(req).adminToken, 'a', { httpOnly: true }).sendStatus(consts.SUCCESS_CODE)
    } catch (e) {
        config.log(e)
    }
}


// if we export a function sooner , we can use this kinda exporting not to export that func again
// module.exports.getManagementDetails = getManagementDetails
// module.exports.updateManagement = updateManagement
// module.exports.subscribe = subscribe
// module.exports.handlePayment = handlePayment
// module.exports.handleOrder = handleOrder

// if we can normally export an object, have to export functions again, cause previous exports will be omitted
module.exports = {authenticate, getListVersions, getManagementDetails, updateManagement, subscribe, createPayment, invitationInfo, getStatics,
    changePassword, logOut}
