module.exports = {

    ERR: 'خطای داخلی',


    LATEST: 'جدیدترین',
    HIGHEST_SUM: 'بالاترین مبلغ',
    HIGHEST_INVITES: 'بالاترین دعوت',
    HIGHEST_ORDERS: 'بالاترین سفارش',


    ORDER_ID: 'order_id',
    X_API_KEY: 'X-API-KEY',
    X_SANDBOX: 'X-SANDBOX',

    ACTIVE_ORDERS: 'فعال',

    ALL: 'همه',
    DELIVERED: 'تحویل شده',
    COMMITTED: 'ثبت شده',
    REJECTED: 'لغو شده',
    ACCEPTED: 'قبول شده',
    SENT: 'ارسال شده',


    PAYED: 1,
    FAILED: 2,
    NOT_VERIFIED: 0,

    TO_BE_PAID: 'در انتظار پرداخت',


    FOOD_LIST_INCOMPATIBILITY: 'عدم تطابق لیست غذا ها، لطفا دوباره سبد خرید را پر کنید',


    ADMIN: 'Admin',
    CUSTOMER: 'Customer',
    DISCOUNT_CODE: 'DiscountCode',
    EVENT: 'Event',
    FOOD: 'Food',
    FOOD_LABEL: 'FoodLabel',
    MESSAGE: 'Message',
    ORDER: 'Order',
    REPORT: 'Report',
    STATICS: 'Statics',
    SUBSCRIPTION: 'Subscription',
    TRANSACTION: 'Transaction',
    USER: 'User',
    VALIDATION_CODE: 'ValidationCode',

    INDEX: 'index',
    SERVICES: 'services',
    ABOUT: 'about',
    EVENTS: 'events',
    INVITATION: 'invitation',
    PAY_RESULT: 'payResult',
    REVIEW: 'review',



    DEMO: 'demo',
    IMO: 'imo',
    HEZAR: 'hezar',



    ANDROID: 'ANDROID',
    WEB: 'WEB',

    AVAILABLE: 'موجود',
    NOT_AVAILABLE: 'ناموجود',

    VALID: 'معتبر',
    BORN_TODAY: 'متولد امروز',
    BANNED: 'مسدود شده',


    TRUE: 'TRUE',
    FALSE: 'FALSE',
    OFF: 'OFF',
    ON: 'ON',

    ONLINE: 'ONLINE',
    CASH: 'CASH',


    PERMISSIONS: 'pr',

    ORDER_LIST: 'asdewq',
    FOOD_LIST: 'qwre',
    CUSTOMER_LIST: 'feca',
    EVENT_LIST: 'asdf',


    DEFAULT: 'default',
    DENIED: 'denied',
    GRANTED: 'granted',

    NOTIFICATION: 'NOTIFICATION',
    A2HS: 'A2HS',




    INCORRECT_PASS: 'Incorrect password',
    INCORRECT_USER: 'Incorrect Username',



    ADMIN_TOKEN : 'at',
    CUSTOMER_TOKEN : 'ct',
    ANDROID_TOKEN : 'ant',

    ROUTE_AUTHENTICATE: '/authenticate',
    ROUTE_REGISTER_PUSH: '/register-push',
    ROUTE_GET_STATICS: '/get-statics',

    ROUTE_GET_REPORTS: '/get-reports',
    ROUTE_GET_MESSAGES: '/get-messages',

    ROUTE_CHECK_TOKEN: '/checkToken',
    ROUTE_CHANGE_PASS: '/change-password',




    ROUTE_SEND_CODE: '/send-code',
    ROUTE_CHECK_CODE: '/check-code',
    ROUTE_ACCEPT_ORDER_STATUS: '/get-accept-order',

    ROUTE_MANAGEMENT: '/management',
    ROUTE_UPDATE_MANAGEMENT: '/update-management',

    ROUTE_LIST_VERSIONS: '/list-versions',

    ROUTE_PAY_PANEL: '/pay',

    ROUTE_GET_CUSTOMERS: '/customer-list',

    ROUTE_NEW_DISCOUNT_CODE: '/new-discount',
    ROUTE_NEW_CUSTOMER: '/new-customer',

    ROUTE_GET_CUSTOMER_INFO: '/customer-info',
    ROUTE_UPDATE_CUSTOMER_INFO: '/update-customer-info',
    ROUTE_BAN_CUSTOMER: '/ban-customer',
    ROUTE_ADD_UPDATE_ADDRESS: '/add-update-address',
    ROUTE_DELETE_ADDRESS: '/delete-address',

    ROUTE_LOGOUT: '/logout',

    ROUTE_GET_ORDERS: '/order-list',
    ROUTE_GET_COMMENTS: '/comment-list',

    ROUTE_NEW_ORDER: '/new-order',
    ROUTE_VALIDATE_DCODE: '/validate-code',
    ROUTE_SUBMIT_ORDER: '/submit-order',
    ROUTE_SCORE_ORDER: '/score-order',

    ROUTE_UPDATE_FOOD: '/update-food',
    ROUTE_DELETE_FOOD: '/delete-food',
    ROUTE_FOOD_GROUP_DISCOUNT: '/food-group-discount',

    ROUTE_UPDATE_MEALS: '/update-meals',
    ROUTE_ACCEPT_ORDER: '/accept-order',

    ROUTE_ADD_FOOD_LABEL: '/add-food-label',
    ROUTE_EDIT_FOOD_LABEL: '/edit-food-label',
    ROUTE_DELETE_FOOD_LABEL: '/delete-food-label',

    ROUTE_GET_FOODS: '/food-list',

    ROUTE_HANDLE_ORDER: '/handle-order',

    ROUTE_GET_EVENTS: '/event-list',
    ROUTE_UPDATE_EVENT: '/update-event',
    ROUTE_DELETE_EVENT: '/delete-event',

    ROUTE_GET_EVENT: '/event',

    ROUTE_PERMISSION_STATE: '/permission-state',
    ROUTE_INVITATION_INFO: '/invitation-info',




    ROUTE_PAYMENT_CALLBACK: '/payment-callback',

    ROUTE_IDPAY_CALLBACK: '/idpay-callback',
    ROUTE_IDPAY_CUSTOMER_CALLBACK: '/payement',


    MONTHS: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],



    ERR_NOT_IN_ACCEPT_ORDER_TIME : 'ثبت سفارش در ساعات کاری مجموعه صورت میگیرد',
    ERR_ORDER_TEMPORARY_OFF : 'ثبت سفارش موقتا صورت نمی گیرد',




    SUCCESS_CODE: 200,
    GONE_CODE: 410,

    CREATED_CODE: 201,

    NO_CONTENT: 205,
    BAD_REQ_CODE: 400,
    UNAUTHORIZED_CODE: 401,
    NOT_FOUND_CODE: 404,
    INT_ERR_CODE: 500
}
