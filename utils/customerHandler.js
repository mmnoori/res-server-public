const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')

const consts = require('./consts')
const errHandler = require('./errHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const dConverter = require('../tools/dateConverter')
const faker = require('../tools/faker')

const admins = require('../routes/admins')
const adminHandler = require('./adminHandler')
const staticsHandler = require('./staticsHandler')




let timer

async function getCustomers(req, res, next) {

    const {skipCount, state, sortBy, searchText, limit} = req.body

    const response = await getCustomersList(multipleHandler.getFlag(req), skipCount, state, sortBy, searchText, limit)

    res.json(response)
}

async function getCustomersList(flagParam, skipCount, state, sortBy, searchText, limit) {

    const [Customer] = multipleHandler.getModel([consts.CUSTOMER], flagParam)

    let query = {}

    // sort
    if (sortBy === consts.LATEST) sortBy = 'created'
    else if (sortBy === consts.HIGHEST_INVITES) sortBy = 'invitedsCount'
    else if (sortBy === consts.HIGHEST_ORDERS) sortBy = 'ordersCount'

    // state
    if (state === consts.VALID) query = { 'banned': false }
    else if (state === consts.BORN_TODAY) {

        const today = dConverter.getTodayForBirth()
        query = { 'birth.day': today.substring(8, 10), 'birth.month': today.substring(5,7) }

    } else if (state === consts.BANNED) query = { 'banned': true}


    // searchText
    if (searchText !== undefined && searchText !== '') {

        const firstChar = searchText.charAt(0)

        if (firstChar === '0') query = {...query, phone: { $regex: '.*' + searchText + '.*' } }
        else if (config.isDigit(firstChar)) query = {...query, code: { $regex: '.*' + searchText + '.*' } }
        else query = {...query, name: { $regex: '.*' + searchText + '.*' } }
    }


    const result = await Customer.find( query, { __v:0, orders:0, inviteds:0, webSubscription:0, androidSubscription:0, banned:0 }, { sort: { [sortBy] : -1 } })
        .skip(skipCount).limit(limit)


    result.forEach(customer => customer.created.substr(0,10) )

    return { customers: result}
}

/*
* send validation code
* */
async function sendCode(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ User, ValidationCode] = multipleHandler.getModel([ consts.USER, consts.VALIDATION_CODE], flagParam)

    if (validator.sendCode(req.body, res)) return

    const { phone } = req.body

    const code = faker.getRandomCode(5)

    let clientIp = (config.isDevelopment)? req.connection.remoteAddress: req.headers['x-real-ip']

    config.log('\nclientIP: ' + clientIp, flagParam)


    // android firefox not sending origin header
    // NOTICE: all the header options can be modified, postman does not send header.origin by default
    // if (!config.isDevelopment && req.headers['origin'] !== config.baseAddress) return res.status(consts.BAD_REQ_CODE).send('Request not valid')



    let user = await User.findOne({ip: clientIp})

    if (user) {
        user.attempts++

        if (user.attempts > 15) {

            user.banned = true
            user.createdAt = Date.now() // update user expiration time
            await user.save()

            return config.log(clientIp +  ' has requested send-code ' + user.attempts + ' times', flagParam)
        } else {

            await user.save().catch(err => config.log(err))
        }
    } else {

        await new User({ _id: new mongoose.Types.ObjectId(), ip: clientIp }).save().catch(err => config.log(err))
    }

    // to test mongoose expire
    // if (!timer) {
    //     let i = 0
    //     timer = setInterval(async () => {
    //         i = i + 5
    //         config.log('\n' + i + ' seconds')
    //
    //         let user = await User.findOne({ip: clientIp})
    //         config.log(user)
    //     }, 5000)
    // }


    config.log(code, flagParam)


    // Sending SMS Code on production
    if (!config.isDevelopment) {

        // const response = await config.postRequest(config.MPAYAMAK_SEND_URL, {username: config.MPayamakUsername, password: config.MPayamakPassword,
        //     to: phone, from: config.MPayamakNumber,
        //     text: 'رستوران بزرگ گیلانه | ' + 'کد ورود شما : ' + code, isFlash: false})
        //
        // config.log('\n')
        // config.log(response.statusCode)
        // config.log(response.data)

        const response = await config.getRequestWithParams(config.KAVE_NEGAR_LOOKUP_URL,
            { token: code, receptor: phone, template: 'v-'+multipleHandler.getHost(flagParam).name })

        config.log('\n Kavenegar Status Code : ')
        config.log(response.statusCode)
        // config.log(response.data)
    }


    // we can use session only for web
    // or jwt for both web and application
    // or simply store temp data in database
    // or use cache


    // using replace instead update to create entire new document and created attr automatically
    await ValidationCode.replaceOne({ phone }, { code, phone }, { upsert: true }).catch(err => config.log(err))


    // to test cacheManager
    // if (!timer) {
    //     let i = 0
    //
    //     timer = setInterval(() => {
    //         i = i + 5
    //         config.log('\n' + i + ' seconds')
    //         config.log(config.cacheManager.get( phone ))
    //     }, 5000)
    // }


    res.sendStatus(consts.SUCCESS_CODE)
}

async function checkCode(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ User, Customer, ValidationCode] = multipleHandler.getModel([ consts.USER, consts.CUSTOMER, consts.VALIDATION_CODE], flagParam)

    // let response = await config.postRequest(config.MPAYAMAK_CHECK_URL, {username: config.MPayamakUsername, password: config.MPayamakPassword,
    //     recID:
    // .recID,
    //     })

    // config.log('\n')
    // config.log(response.statusCode)
    // config.log(response.data)


    const { code, phone, android } = req.body


    // NOTE: findOneAndUpdate returns object before update by default, but you can set new: true option to get new document
    const savedCode = await ValidationCode.findOneAndUpdate({code}, {validated: true}, { new: true })

    if (!savedCode || savedCode.phone !== phone) return res.status(consts.BAD_REQ_CODE).send('کد وارد شده معتبر نیست')



    const customer = await Customer.findOne({ phone }, { __v: 0, created: 0})

    if (customer) { // if customer was registered

        if (android) signToken({ phone }, req,  res, customer)
        else signCookie({ phone }, req, res, customer)

    } else { // need to signup
        res.status(consts.NOT_FOUND_CODE).send('کاربر پیدا نشد')
    }
}

async function newCustomer(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer, ValidationCode, Admin] = multipleHandler.getModel([ consts.CUSTOMER, consts.VALIDATION_CODE, consts.ADMIN], flagParam)

    let issue = false

    let { phone, name, address, day, month, year, home, location, android } = req.body


    const validationCode = await ValidationCode.findOne({ phone })
    if (!validationCode || validationCode.phone !== phone) return res.status(consts.BAD_REQ_CODE).send('PHONE NOT VALIDATED')


    // add zero if day is single digit
    if (day.length === 1) day = '0'+ day

    // change month to number
    month = config.monthToNumber(month)

    const admin = await Admin.findOne()

    const code = faker.getRandomCode(6)

    const customer = new Customer({
        _id: new mongoose.Types.ObjectId(),
        code,
        name,
        phone,
        credit: admin.dfo, // using dfo in credit
        birth: { day, month, year },
        addresses: [
            {
                address : address,
                location: {lng: location.lng, lat: location.lat},
                telePhone: home,
            }
        ]
    })

    await customer.save().catch(err => {
        errHandler(err, res)
        issue = true
    })

    if (issue) return

    staticsHandler.incrementCustomer(flagParam)

    if (android) signToken({ phone, code }, req,  res, customer)
    else signCookie({ phone, code }, req, res, customer)
}

/* for web */
function signCookie(payload, req, res, response) {

    const host = multipleHandler.getHost(req)

    // Issue token
    const token = jwt.sign(payload, host.jwtSecret, { expiresIn: '7d' })

    try {
        res.cookie(host.customerToken, token, {
            httpOnly: true, // Flags the cookie to be accessible only by the web server
            maxAge:  7 * 24 * 3600 * 1000  // cookie will be removed after 7 days
        }).status(consts.SUCCESS_CODE).json(response)

    } catch (e) {
        config.log(e)
    }
}

/* for android */
function signToken(payload, req, res, response) {

    const host = multipleHandler.getHost(req)

    const token = jwt.sign(payload, host.jwtSecret, { expiresIn: '180d' })
          // response is customer object queried from db
    res.json({...response._doc, token})
}

async function getInfo(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer] = multipleHandler.getModel([ consts.CUSTOMER], flagParam)

    // we are passed here from isLogged or client is requesting info
    const phone = (req.phone)? req.phone: req.body.phone

    // const customer = await Customer.findOne({phone}, { __v: 0, owner: 0, inviteds: 0, orders: 0, webSubscription: 0})
    // populate() second argument is field name syntax/field selection


    // If a callback is passed, the aggregate is executed and a Promise is returned. If a callback is not passed, the aggregate itself is returned.
    Customer.aggregate([ {$match: { phone }}
        , { $project: {
                code: 1,
                name: 1,
                phone: 1,
                birth: 1,
                addresses: 1,
                credit: 1,
                invitedsCount: 1,
                ordersCount: 1,
                banned: 1,
                created: 1,
                usedDiscountsCount: { $cond: { if: { $isArray: "$usedDiscounts" }, then: { $size: "$usedDiscounts" }, else: 0} }
            }}
    ]).then(result => {
        // config.log(result)
        res.json(result[0])
    })

}

async function updateInfo(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer] = multipleHandler.getModel([ consts.CUSTOMER], flagParam)

    let issue = false
    const {name} = req.body

    let customer = await Customer.findOne({phone: req.phone}).populate('orders', '-__v -owner -inviteds')

    customer.name = name

    await customer.save(err => {
        if (err) {
            issue = true
            errHandler(err, res)
        }
    })

    if (issue) return

    res.json(customer)
}

async function addUpdateAddress(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer] = multipleHandler.getModel([ consts.CUSTOMER], flagParam)

    let issue = false
    let params = req.body

    let customer = await Customer.findOne({phone: params.phone}, { __v: 0, created: 0})

    if (params._id) { // updating an existing address

        customer.addresses.forEach(address => {

            if (address._id == params._id) { // === won't work when comparing mongoDB ObjectId
                address.address = params.address
                address.telePhone = params.telePhone
                address.location = {lng: params.location.lng, lat: params.location.lat}
            }
        })

    } else { // creating new address

        customer.addresses.push({
            address: params.address,
            telePhone: params.telePhone,
            location: {lng: params.location.lng, lat: params.location.lat}
        })
    }

    await customer.save().catch(e => {
        config.log(e)
        issue = true
    })

    if (issue) return res.status(consts.BAD_REQ_CODE).send('حداکثر 3 آدرس میتوانید وارد کنید')

    res.json(customer)
}

async function deleteAddress(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer] = multipleHandler.getModel([ consts.CUSTOMER], flagParam)

    let issue = false
    const {_id} = req.body
                                            // req.phone from authentication
    let customer = await Customer.findOne({phone: req.phone}, { __v: 0, created: 0})

    // finding address by _id and deleteing it
    customer.addresses.forEach((address, index) => {


        if (address._id == _id) // === won't work when comparing mongoDB ObjectId
            customer.addresses.splice(index, 1)
    })

    await customer.save().catch(e => config.log(e))

    res.json(customer)
}

/*
* if user denies permission request, we save a session not to ask again in future hours
* this api handles both set and get for notification and a2hs permission state
* */
function permissionStateManager(req, res, next) {

    const {state, type} = req.body


    // state is always consts.DENIED if sent by client
    if (state === consts.DENIED) {

        if (type === consts.NOTIFICATION) {
            req.session.notificationPermission = state
            return res.sendStatus(consts.SUCCESS_CODE)
        }
        else if (type === consts.A2HS) {
            req.session.a2hsPermission = state
            return res.sendStatus(consts.SUCCESS_CODE)
        }
    }

    // sending current state to user

    if (type === consts.NOTIFICATION) {
        if (req.session.notificationPermission) res.send(consts.DENIED)
        else res.send(consts.DEFAULT)
    }
    else if (type === consts.A2HS) {
        if (req.session.a2hsPermission) res.send(consts.DENIED)
        else res.send(consts.DEFAULT)
    }

}

/*
* save public and private subscriptions
* */
async function registerPushSubscription(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Subscription, Customer] = multipleHandler.getModel([ consts.SUBSCRIPTION, consts.CUSTOMER], flagParam)

    let issue = false
    let updatingSubscription = false

    const {webSubscription, androidSubscription, phone} = req.body

    if (webSubscription) {

        // NOTE: findOneAndUpdate returns object before update by default, but you can set new: true option to get new document
        // so if creating a new subscription undefined is returned
        let subscription = await Subscription.findOneAndUpdate({subscription: webSubscription}, {
            platform: consts.WEB,
            subscription: webSubscription,
            createdAt: Date.now()
        },{upsert: true})

        // if created new subscription ,query subscription to get its _id
        if (!subscription) subscription = await Subscription.findOne({subscription: webSubscription})
        else updatingSubscription = true

        if (phone) { // update customer

            let customer = await Customer.findOne({phone})
            customer.webSubscription = subscription._id
            await customer.save().catch(err => config.log(err))

        } else if (updatingSubscription) { // when updating a public registration -> check if subscription was for a customer before => wipe it clean

            let customer = await Customer.findOne({webSubscription: subscription._id})
            if (customer) {
                customer.webSubscription = undefined
                await customer.save().catch(err => config.log(err))
            }

        }

    } else if (androidSubscription) {

        let subscription = await Subscription.findOneAndUpdate({subscription: androidSubscription}, {
            platform: consts.ANDROID,
            subscription: androidSubscription,
            createdAt: Date.now()
        },{upsert: true})

        if (phone) { // update customer
            let customer = await Customer.findOne({phone})
            customer.androidSubscription = subscription._id
            await customer.save().catch(err => config.log(err))
        }
    }

    res.sendStatus(consts.SUCCESS_CODE)
}


async function banCustomer(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer] = multipleHandler.getModel([ consts.CUSTOMER], flagParam)

    const {_id, banned, skipCount, state, sortBy, searchText, limit} = req.body

    await Customer.updateOne({_id}, {banned})

    const response = await getCustomersList(flagParam, skipCount, state, sortBy, searchText, limit)

    res.json(response)
}



function logOut(req, res) {

    // config.log('here')

    try {
        res.cookie(multipleHandler.getHost(req).customerToken, 'a', { httpOnly: true }).sendStatus(consts.SUCCESS_CODE)
    } catch (e) {
        config.log(e)
    }
}









module.exports = {getCustomers, sendCode, checkCode, newCustomer, logOut, getInfo, addUpdateAddress, deleteAddress, updateInfo,
    registerPushSubscription, permissionStateManager, banCustomer, getCustomersList}
