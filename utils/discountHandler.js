const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const webpush = require("web-push")

const consts = require('./consts')
const errHandler = require('./errHandler')
const customerHandler = require('./customerHandler')
const notificationHandler = require('./notificationHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const dConverter = require('../tools/dateConverter')
const faker = require('../tools/faker')

const admins = require('../routes/admins')




async function newDiscountCode(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [ DiscountCode, Message, Report, Customer ] =
        multipleHandler.getModel([ consts.DISCOUNT_CODE, consts.MESSAGE, consts.REPORT, consts.CUSTOMER], flagParam)


    let issue = false
    const {code, percent, minPrice, occasion, expires, customers, searchText, state} = req.body


    const english = /^[A-Za-z0-9]*$/

    if (english.test(code) === false)
        return res.status(consts.BAD_REQ_CODE).send('از اعداد و حروف انگلیسی  برای کد استفاده کنید')


    let discountCode = await DiscountCode.findOne({code})

    if (discountCode) return res.status(consts.BAD_REQ_CODE).send('این کد تخفیف قبلا ثبت شده است')


    let isValid = true

    // just need to have one non-digit char
    for (let i = 0; i < code; i++)
        if (config.isDigit(i) === false) {
            isValid = false
            break
        }

    if (!isValid) return res.status(consts.BAD_REQ_CODE).send('کد باید حداقل شامل یک حرف باشد')

    const expiration = expires.year + '-' + expires.month + '-' + expires.day

    isValid = checkExpiration(expiration)

    if (!isValid) return res.status(consts.BAD_REQ_CODE).send('تاریخ انقضا گذشته است')


    let customerCount
    let owners = customers // it maybe 'ALL' either an array of codes
    let tempCustomers

    if (customers === consts.ALL) {
        customerCount = 'همه کاربران'

        // if list was filtered by search or state
        if (state || searchText) {

            let query

            if (state === consts.BORN_TODAY) {

                const today = dConverter.getTodayForBirth()
                query = { 'birth.day': today.substring(8, 10), 'birth.month': today.substring(5,7) }
            }

            if (searchText !== undefined && searchText !== '') {

                const firstChar = searchText.charAt(0)

                if (firstChar === '0') query = {...query, phone: { $regex: '.*' + searchText + '.*' } }
                else if (config.isDigit(firstChar)) query = {...query, code: { $regex: '.*' + searchText + '.*' } }
                else query = {...query, name: { $regex: '.*' + searchText + '.*' } }
            }

            tempCustomers = await Customer.find( {...query, banned: false}, { code:1, webSubscription:1, androidSubscription:1 }).populate('androidSubscription').populate('webSubscription')


            owners = []

            for (let i = 0; i < tempCustomers.length; i++)
                owners.push(tempCustomers[i].code)

            customerCount = tempCustomers.length + ' کاربر'
        }

    } else // if customers wis an array
        customerCount = customers.length + ' کاربر'


    discountCode = new DiscountCode({
        _id: new mongoose.Types.ObjectId(),
        code,
        discount: percent,
        minOrderPrice: (minPrice !== '')? minPrice: null,
        occasion: (occasion !== '')? occasion: null,
        expires: expiration,
        owner: owners
    })

    discountCode.markModified('owner') // it is mixed type

    await discountCode.save().catch(err => {
        errHandler(err, res)
        issue = true
    })

    if (issue) return



    res.json({ count: customerCount })

    // NOTE: do all shits after this asyncly, because express stops next requests until this request if finished


    const day = (expires.day.charAt(0) === '0')? expires.day.charAt(1) : expires.day
    const month = config.monthToString(expires.month)

    const title = percent + ' درصد تخفیف ویژه'
    const mPrice = (minPrice)? ' برای سفارش بالای ' + config.getThousandToman(minPrice) + ' هزار تومان': ''
    const discountOccasion = (occasion)? ' به مناسبت ' + occasion : ''

    const content = 'کد تخفیف ' + percent + ' درصدی ' + code + mPrice + discountOccasion + ' به شما هدیه داده شده، ' +
        'مهلت استفاده این کد تا پایان ' + day + ' ' + month + ' می باشد.'

    const reportDesc = 'کد تخفیف ' + percent + ' درصدی ' + code + mPrice + discountOccasion + ' به ' + customerCount + ' ارسال شد. ' +
        'مهلت استفاده این کد تا پایان ' + day + ' ' + month + ' می باشد.'


    // save to this reports
    await new Report({
        title: 'ارسال کد تخفیف',
        desc: reportDesc
    }).save(err => {
        if (err) config.log(err)
    })





    // sending notification for all subscriptions and everyone(currently registered or not) can use it
    // messages will be saved for everyone either, if we're sending discount for ALL
    if (customers === consts.ALL) {

        // if no filter is sent, send push for all push subscriptions
        if (!state && !searchText) {

            await new Message({
                title,
                desc: content,
                owner: consts.ALL
            }).save(err => {
                if (err) config.log(err)
            })

            notificationHandler.sendToAll({title, content}, flagParam)
            return

        }

    } else {                                             // customers is an array of codes here
        owners = await Customer.find({ 'code': { $in: customers} }, { code: 1, webSubscription:1, androidSubscription:1 })
            .populate('androidSubscription').populate('webSubscription')
    }


    // if customers are selected or some filters are sent,
    for (let i = 0; i < owners.length; i++) {

        await new Message({
            title,
            desc: content,
            owner: owners[i].code
        }).save(err => {
            if (err) config.log(err)
        })

        await notificationHandler.sendNotif(owners[i], {title, content}, flagParam)

        // if (owners[i].webSubscription) {
        //
        //     await webpush.sendNotification(owners[i].webSubscription.subscription, JSON.stringify({title, body: content})).catch(err => {
        //             if (err.statusCode === consts.GONE_CODE) {
        //                 config.log('removing an unavailable webSubscription')
        //
        //                 owners[i].webSubscription = undefined
        //                 owners[i].save(err => {
        //                     if(err) config.log(err)
        //                 })
        //
        //                 Subscription.deleteOne({_id: webSubscription._id}).catch(err => config.log(err))
        //             }
        //         }
        //     )
        // }
    }

}

async function checkDiscountCode(req, res) {

    const {code, dPrice} = req.body

    const isDiscountCode = config.includesLetter(code) // discountCode or customerCode

    if (isDiscountCode){
        let discountCode = await checkCodeValidity(code, dPrice, req, res)

        if (!discountCode) return

        discountCode = await canCustomerUseDiscount(discountCode, dPrice, req, res)

        if (!discountCode) return

        res.json({percent: discountCode.discount})

    } else { // user invitation code

        let {discount, inviter} = await checkInviterCodeValidity(code, dPrice, req, res)

        if (!discount) return

        res.json({percent: discount})
    }

}

async function checkInviterCodeValidity(inviterCode, dPrice, req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer, Admin, Order] = multipleHandler.getModel([ consts.CUSTOMER, consts.ADMIN, consts.ORDER], flagParam)

    // req.code and req.phone are set after jwt authentication
    if (inviterCode === req.code) {
        res.status(consts.BAD_REQ_CODE).send('نمی توانید از کد خودتان استفاده کنید')
        return false
    }

    const inviter = await Customer.findOne({code: inviterCode})

    if (!inviter) {
        res.status(consts.BAD_REQ_CODE).send('این کد تخفیف وجود ندارد')
        return false
    }

    // estimatedDocumentCount counts all documents in a collection
    const notRejectedOrdersCount = await Order.countDocuments({phone: req.phone, state: {$ne: consts.REJECTED}})

    if (notRejectedOrdersCount !== 0) {
        res.status(consts.BAD_REQ_CODE).send('این کد برای اولین سفارش می باشد')
        return false
    }

    const admin = await Admin.findOne()

    if (admin.invitation.minPrice > dPrice) {
         res.status(consts.BAD_REQ_CODE)
             .send('این کد تخفیف برای سفارش های بالای ' + config.getThousandToman(admin.invitation.minPrice) + ' هزار تومن می باشد')
         return false
    }

    return {discount: admin.invitation.discount, inviter}

}


// just checks if there is any valid discountCode and that customer can use it
async function checkCodeValidity(code, dPrice, req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [ DiscountCode] = multipleHandler.getModel([ consts.DISCOUNT_CODE], flagParam)

    const discountCode = await DiscountCode.findOne({code})

    if (!discountCode) {
        res.status(consts.BAD_REQ_CODE).send('کد تخفیف ارسالی وجود ندارد')
        return false
    }

    const expirationCheck = checkExpiration(discountCode.expires)

    if (expirationCheck === false) {
        res.status(consts.BAD_REQ_CODE).send('کد تخفیف منقضی شده است')
        return false
    }

    const canUse = await canCustomerUseDiscount(discountCode, dPrice, req, res)

    if (!canUse) return

    return discountCode
}


function checkExpiration(expiration) {

    // check if discountCode is expired
    const now = dConverter.getLiveDate()
    const toDay = now.substr(0, 10)

    // seems js can compare nnnn-nn-nn format
    // config.log(now > discountCode.expires)
    if (toDay > expiration) return false

    return true
}
/*
* @param {number} if for getting req.phone
* @returns {number}
* */
async function canCustomerUseDiscount(discountCode, dPrice, req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Customer] = multipleHandler.getModel([ consts.CUSTOMER], flagParam)

    // if there was any minOrderPrice limitation, compare it
    if (discountCode.minOrderPrice && discountCode.minOrderPrice > dPrice) {
        res.status(consts.BAD_REQ_CODE).send('این کد برای سفارش های بالای ' + config.getThousandToman(discountCode.minOrderPrice) + ' هزار تومن است')
        return false
    }

    let customer = await Customer.findOne({phone: req.phone})


    let used = isCodeUsedBefore(customer, discountCode.code, res)

    if (used) return false



    if (discountCode.owner === consts.ALL) {
        return discountCode

    } else { // if we have an array of owners code

        for (let i = 0; i < discountCode.owner.length; i++)
            if (discountCode.owner[i] === customer.code) return discountCode // if customer was an owner of discount

        res.status(consts.BAD_REQ_CODE).send('این کد برای شما قابل استفاده نیست')
        return false
    }
}

function isCodeUsedBefore(customer, code, res) {

    for (let i = 0; i < customer.usedDiscounts.length; i++)
        if (customer.usedDiscounts[i] === code) { // code was used before
            res.status(consts.BAD_REQ_CODE).send('این کد قبلا استفاده شده است')
            return true
        }
    return false
}





module.exports = { newDiscountCode, checkDiscountCode, checkCodeValidity, canCustomerUseDiscount, checkInviterCodeValidity}
