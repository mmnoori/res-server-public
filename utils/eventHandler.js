const mongoose = require('mongoose')
const {parse} = require('node-html-parser')

const adminHandler = require('./adminHandler')
const notificationHandler = require('./notificationHandler')
const consts = require('./consts')
const errHandler = require('./errHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const fileManager = require('../tools/fileManager')



async function getEvents(req, res) {

    let response = {
        events: [],
        eventListVersion: 0,
    }

    const {skipCount, limit, exceptUri} = req.body

    response = await getEventsList(skipCount, limit, exceptUri, multipleHandler.getFlag(req))

    res.status(consts.SUCCESS_CODE).json(response)
}

async function getEventsList(skipCount, limit, exceptUri, flagParam) {

    const [Event] = multipleHandler.getModel([consts.EVENT], flagParam)


    // NOTE: if skipCount and limit and exceptUri are undefined, query will be executed normally without errors
    const result = await Event.find({ uri: { $ne: exceptUri } }, { __v: 0, created: 0 }, { sort: { created : -1 } }).skip(skipCount).limit(limit)

    const count = await Event.estimatedDocumentCount()

    return { events: result, count}
}

/*
* add/edit event
* */
async function updateEvent(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [Event] = multipleHandler.getModel([consts.EVENT], flagParam)

    let issue = false

    const params = req.body
    const {title, notification, _id, content, changeImg, limit} = JSON.parse(params.event)

    let uri = ''

    for (let i = 0; i < title.length; i++) {

        if (title.charAt(i) === ' ') uri += '-'
        else uri += title.charAt(i)
    }

    // return console.log(parse(content))

    const desc = config.getEventTextDescription(parse(content).text, 100)

    // when using multer disk storage , this function will be executed only if file was uploaded and saved successfully
    // but when using multer memory storage , this function will be executed anyway
    const isImgUploaded = (req.file)

    const imgName = (isImgUploaded)? fileManager.setFileName(req.file.fieldname, req.file.mimetype): null // req.file if added to req by multer after file successfully uploaded


    if (_id) { // editing

        let eventToUpdate = await Event.findOne({_id})

        eventToUpdate.title = title
        eventToUpdate.uri = uri
        eventToUpdate.content = content
        eventToUpdate.notification = notification
        eventToUpdate.desc = desc

        if (changeImg) {

            if (eventToUpdate.imgSrc) // if already had image -> delete it
                fileManager.remove(multipleHandler.getImagesPath(flagParam) + eventToUpdate.imgSrc)

            eventToUpdate.imgSrc = imgName
        }

        await eventToUpdate.save().catch(err => {
            res.status(consts.BAD_REQ_CODE).send('نام رویداد تکراری می باشد')
            issue = true
        })

    } else { // creating

        await new Event({
            title,
            desc,
            uri,
            content,
            notification,
            imgSrc: imgName,
        })
            .save().catch(err => {
                res.status(consts.BAD_REQ_CODE).send('نام رویداد تکراری می باشد')
                issue = true

                // dont need to delete when using memory storage
                // if (req.file) // delete saved image and return
                //     fileManager.remove(req.file.path)
            })
    }

    if (issue) return
    else if (isImgUploaded) fileManager.saveEventImg(req.file.buffer, imgName, flagParam) // save image if there was no issue saving food and an img was uploaded


    const response = await getEventsList(0, limit, undefined, flagParam)

    res.json(response)

    // sending notification asyncronously
    // NOTE: null is NOT equal with undefined
    if (_id === null && notification) {

        notificationHandler.sendToAll({title, content: desc,
            image:multipleHandler.getHost(req).base+'/images/'+imgName,
            url: multipleHandler.getHost(req).base+'/events/'+uri}, flagParam)
    }
}

async function deleteEvent(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [Event] = multipleHandler.getModel([consts.EVENT], flagParam)

    let query

    let {_id, limit} = req.body

    let event = await Event.findOne({_id})

    if (event.imgSrc)
        fileManager.remove(multipleHandler.getImagesPath(flagParam) + event.imgSrc)

    await Event.deleteOne({ _id}, function (err) {})


    const response = await getEventsList(0, limit, undefined, flagParam)

    res.json(response)
}

async function getEvent(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [Event] = multipleHandler.getModel([consts.EVENT], flagParam)

    const {uri} = req.body

    const event = await Event.findOne({uri}, { __v: 0, created: 0, notification: 0 })

    res.json(event)
}






module.exports = { getEvents, updateEvent, deleteEvent, getEvent}
