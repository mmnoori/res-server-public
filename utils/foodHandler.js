const mongoose = require('mongoose')

const adminHandler = require('./adminHandler')
const consts = require('./consts')
const errHandler = require('./errHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const fileManager = require('../tools/fileManager')

const admins = require('../routes/admins')



async function getFoods(req, res, argAdmin) {

    // console.log(`req.headers = \n`)
    // console.log(req.headers)

    const [Admin, Food, FoodLabel] = multipleHandler.getModel([consts.ADMIN, consts.FOOD, consts.FOOD_LABEL], multipleHandler.getFlag(req))

    let response = {
        foods: [],
        foodLabels: [],
        flv: 0,
        address: '',
        phone: '',
        meals: {
            hasTwoMeals: false,
        },
        acceptOrder: {},
        invitation: {
            discount: 0,
            minPrice: 0,
            credit: 0
        }
    }

    let query = {}

    const {label, state} = req.body

    // config.log('\n')
    // config.log(`label: `)
    // config.log(label)
    //
    // config.log(`state: `)
    // config.log(state)

    if (label !== undefined && label !== consts.ALL) query = {label}

    if (state === consts.AVAILABLE) query = {...query, available: true}
    else if (state === consts.NOT_AVAILABLE) query = {...query, available: false}

    response.foods = await Food.find(query, { __v: 0 })
    response.foodLabels = await FoodLabel.find({}, { __v: 0 })

    let admin  // when we don't call getFoods manually with argAdmin, it is called by router and argAdmin is actually next
    if (argAdmin._id) admin = argAdmin
    else admin = await Admin.findOne({})

    response.meals = config.getMealsFromAdmin(admin)

    response.flv = admin.flv
    response.acceptOrder = admin.acceptOrder
    response.address = admin.address
    response.location = admin.location
    response.phone = admin.phone

    response.invitation.discount = admin.invitation.discount
    response.invitation.minPrice = admin.invitation.minPrice
    response.invitation.credit = admin.invitation.credit

    res.status(consts.SUCCESS_CODE).json(response)
}

/*
* create/edit food
* */
async function updateFood(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [Admin, Food, FoodLabel] = multipleHandler.getModel([consts.ADMIN, consts.FOOD, consts.FOOD_LABEL], flagParam)


    let issue = false

    let params = req.body
    const {_id, name, des, label, price, discount, available, suggested, changeImg} = JSON.parse(params.food)

    // when using multer disk storage , this function will be executed only after sent file was uploaded and saved successfully
    // but when using multer memory storage , this function will be executed anyway
    let isImgUploaded = (req.file)

    let imgName = (isImgUploaded)? fileManager.setFileName(req.file.fieldname, req.file.mimetype): null // req.file if added to req by multer after file successfully uploaded

    let foodLabel = await FoodLabel.findOne({name: label})

    if(!foodLabel) return res.status(consts.BAD_REQ_CODE).json('دسته غذایی تعیین شده پیدا نشد')


    const dPrice = (discount === 0)? price: price - Math.round((price/100*discount))

    if (_id) { // editing

        let foodToUpdate = await Food.findOne({_id})

        foodToUpdate.name = name
        foodToUpdate.des = des
        foodToUpdate.label = label
        foodToUpdate.price = price
        foodToUpdate.discount = discount
        foodToUpdate.dPrice =  dPrice
        foodToUpdate.available = available
        foodToUpdate.suggested = suggested

        if (changeImg) {

            if (foodToUpdate.imgSrc) // if already had image -> delete it
                // fileManager.remove(config.imagesPath + foodToUpdate.imgSrc)
                fileManager.remove(multipleHandler.getImagesPath(flagParam) + foodToUpdate.imgSrc)

            foodToUpdate.imgSrc = imgName
        }

        // using save() function instead of update(), according to mongoose docs :
        // The save() function is generally the right way to update a document with Mongoose.
        // If you need save middleware and full validation, first query for the document and then save() it.
        await foodToUpdate.save().catch(err => {
            errHandler('مشکلی در ذخیره غذا وجود دارد', res)
            issue = true
        })

    } else { // creating

        await new Food({
            _id: new mongoose.Types.ObjectId(),
            name,
            des,
            label,
            price,
            discount,
            dPrice,
            available,
            suggested,
            imgSrc: imgName,
        })
            .save().catch(err => {
                res.status(consts.BAD_REQ_CODE).send('غذایی با این نام قبلا ثبت شده است')
                issue = true

                // dont need to delete when using memory storage
                // if (req.file) // delete saved image and return
                //     fileManager.remove(req.file.path)
            })
    }

    if (issue) return
    else if (isImgUploaded) fileManager.saveImg(req.file.buffer, imgName, flagParam) // save image if there was no issue saving food and an img was uploaded

    let admin = await admins.updateAdminListVersion([consts.FOOD_LIST], flagParam)

    await getFoods(req, res, admin)
}

async function deleteFood(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [ Food ] = multipleHandler.getModel([ consts.FOOD ], flagParam)

    let query

    const { _id } = req.body

    let food = await Food.findOne({_id})

    if (food.imgSrc)
        fileManager.remove(multipleHandler.getImagesPath(flagParam) + food.imgSrc)

    await Food.deleteOne({ _id }, function (err) {})


    let admin = await admins.updateAdminListVersion([consts.FOOD_LIST], flagParam)

    await getFoods(req, res, admin)
}

async function setGroupFoodDiscount(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Food] = multipleHandler.getModel([consts.FOOD], flagParam)


    const params = req.body

    let query = {}

    if (params.discountLabel !== consts.ALL)
        query = {...query, label: params.discountLabel}

    if (params.suggestedOnly)
        query = {...query, suggested: true}

    if (params.availableOnly)
        query = {...query, available: true}

    let result = await Food.updateMany(query, { $set: { discount: params.discount } })

    config.log('\n')
    config.log(result)

    let admin = await admins.updateAdminListVersion([consts.FOOD_LIST], flagParam)

    await getFoods(req, res, admin)
}

async function addFoodLabel(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Admin, Food, FoodLabel] = multipleHandler.getModel([consts.ADMIN, consts.FOOD, consts.FOOD_LABEL], flagParam)


    let issue = false

    let params = req.body

    let name = params.name

    let foodLabel = new FoodLabel({
        _id: new mongoose.Types.ObjectId(),
        name: name,
    })

    let result = await foodLabel.save().catch(err => {
        errHandler(err, res)
        issue = true
    })

    if (issue) return

    let admin = await admins.updateAdminListVersion([consts.FOOD_LIST], flagParam)

    await getFoods(req, res, admin)
}

async function editFoodLabel(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Admin, Food, FoodLabel] = multipleHandler.getModel([consts.ADMIN, consts.FOOD, consts.FOOD_LABEL], flagParam)


    let issue = false

    let {newName, prevName} = req.body

    let foodLabel = await FoodLabel.findOne({name: prevName})


    await Food.updateMany({label: prevName}, { $set: { label: newName } })

    foodLabel.name = newName

    await foodLabel.save().catch(err => {
        errHandler(err, res)
        issue = true
    })

    if (issue) return

    let admin = await admins.updateAdminListVersion([consts.FOOD_LIST], flagParam)

    await getFoods(req, res, admin)
}

async function deleteFoodLabel(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Admin, Food, FoodLabel] = multipleHandler.getModel([consts.ADMIN, consts.FOOD, consts.FOOD_LABEL], flagParam)


    let params = req.body

    await FoodLabel.deleteOne({name: params.name} ,(err, foodLabel) => {})

    let foods = await Food.find({label: params.name}, function (err) {})

    foods.map(food => {
        if (food.imgSrc) // if has image -> delete it
            fileManager.remove(config.imagesPath + food.imgSrc)

    })

    await Food.deleteMany({label: params.name}, function (err) {})

    let admin = await admins.updateAdminListVersion([consts.FOOD_LIST], flagParam)

    await getFoods(req, res, admin)
}

async function updateMeals(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Admin, Food, FoodLabel] = multipleHandler.getModel([consts.ADMIN, consts.FOOD, consts.FOOD_LABEL], flagParam)


    const params = req.body

    // clearTimeout(checkMealTimeout)
    multipleHandler.clearMealsCheck(flagParam)


    let admin = await Admin.findOne({})

    admin.meals.hasTwoMeals = params.hasTwoMeals

    admin.meals.lunch.startHour = (params.lsh === '0' || (params.lsh < 10 && params.lsh.substr(0,1) !== '0'))? '0'+params.lsh : params.lsh
    admin.meals.lunch.startMin= (params.lsm === '0' || (params.lsm < 10 && params.lsm.substr(0,1) !== '0'))? '0'+params.lsm : params.lsm
    admin.meals.lunch.endHour = (params.leh === '0' || (params.leh< 10 && params.leh.substr(0,1) !== '0'))? '0'+params.leh : params.leh
    admin.meals.lunch.endMin = (params.lem === '0' || (params.lem < 10 && params.lem.substr(0,1) !== '0'))? '0'+params.lem : params.lem

    admin.meals.dinner.startHour = (params.dsh === '0' || (params.dsh < 10 && params.dsh.substr(0,1) !== '0'))? '0'+params.dsh : params.dsh
    admin.meals.dinner.startMin = (params.dsm === '0' || (params.dsm < 10 && params.dsm.substr(0,1) !== '0'))? '0'+params.dsm : params.dsm
    admin.meals.dinner.endHour  = (params.deh  === '0' || (params.deh < 10 && params.deh.substr(0,1) !== '0'))? '0'+params.deh : params.deh
    admin.meals.dinner.endMin  = (params.dem === '0' || (params.dem < 10 && params.dem.substr(0,1) !== '0'))? '0'+params.dem : params.dem

    const meals = config.getMealsFromAdmin(admin)


    await admins.updateAdminListVersion([consts.FOOD_LIST], flagParam)

    // config.checkMeal(0, admin, fAdmin => {
    //
    //     fAdmin.save().catch()
    //     res.json({acceptOrder: fAdmin.acceptOrder, meals})
    // })

    await multipleHandler.checkMeals(0, admin, fAdmin => {

        fAdmin.save().catch()
        res.json({acceptOrder: fAdmin.acceptOrder, meals})

    }, flagParam)

}

async function getMenu(req) {

    const [Food] = multipleHandler.getModel([consts.FOOD], multipleHandler.getFlag(req))

    return await Food.find({available: true}, { __v: 0 })
}











module.exports = { getFoods, addFoodLabel, editFoodLabel, deleteFoodLabel, updateFood, deleteFood,
    setGroupFoodDiscount, updateMeals, getMenu}


