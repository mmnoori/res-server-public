const mongoose = require('mongoose')

const consts = require('./consts')
const errHandler = require('./errHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const dConverter = require('../tools/dateConverter')
const faker = require('../tools/faker')



async function getMessages(req, res, next) {

    const {skipCount, limit} = req.body

    // await new Promise((res, reject) => {
    //     setTimeout(() => {
    //         res()
    //     }, 3000)
    // })

    // req.code from jwt auth
    const response = await getMessagesList(skipCount, limit, req.code, multipleHandler.getFlag(req))

    res.json(response)
}

async function getMessagesList(skipCount, limit, code, flagParam) {

    const [ Message] = multipleHandler.getModel([ consts.MESSAGE], flagParam)

    let query = {}

    const result = await Message.find( {$or:[ {'owner': consts.ALL}, {'owner': code} ] }, { __v:0, }, { sort: {created: -1} } )
        .skip(skipCount).limit(limit)


    return { messages: result}
}










module.exports = {getMessages, getMessagesList, }
