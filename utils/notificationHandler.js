const mongoose = require('mongoose')
const webpush = require("web-push")

const consts = require('./consts')
const errHandler = require('./errHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')




async function sendToAll(object, flagParam) {

    const [ Subscription] = multipleHandler.getModel([ consts.SUBSCRIPTION], flagParam)

    const subscriptions = await Subscription.find()

    for (let i = 0; i < subscriptions.length ; i++) {

        // send push
        if (subscriptions[i].platform === consts.WEB)

            await webpush.sendNotification(subscriptions[i].subscription, JSON.stringify({
                ...getPushOptions(object, flagParam),
                requireInteraction: true,
            }), {vapidDetails: multipleHandler.getVapidDetails(flagParam)}).catch(err => {

                    if (err) config.log(err)

                    if (err.statusCode === consts.GONE_CODE) {

                        config.log('removing an unavailable webSubscription')
                        // deleting subscription
                        Subscription.deleteOne({_id: subscriptions[i]._id}).catch(err => config.log(err))
                    }
                }
            )
        else if (subscriptions[i].platform === consts.ANDROID){ // android

            // await config.postRequest('https://api.pushe.co/v2/messaging/notifications/', {
            //     app_ids: ['com.restaurant_app'],
            //     filters: {pushe_id: [subscription.subscription]},
            //     data: {
            //         title: notify.title,
            //         content: notify.body
            //     }
            // }, {authorization: 'TOKEN ' + config.PUSHE_TOKEN,})
        }

    }

}

// NOTE: webSubscription and androidSubscription must be populated on customer document
async function sendNotif(customer, object, flagParam) {

    const [ Subscription] = multipleHandler.getModel([ consts.SUBSCRIPTION], flagParam)

    if (customer.webSubscription)
        await webpush.sendNotification( customer.webSubscription.subscription, JSON.stringify(getPushOptions(object, flagParam)),
            {vapidDetails: multipleHandler.getVapidDetails(flagParam)})
            .catch(err => {

                if (err) config.log(err)

                if (err.statusCode === consts.GONE_CODE) {
                    config.log('removing an unavailable webSubscription')

                    customer.webSubscription = undefined
                    customer.save(err => {
                        if(err) config.log(err)
                    })

                    Subscription.deleteOne({_id: customer.webSubscription._id}).catch(err => config.log(err))
                }
            }
        )
    else if (customer.androidSubscription) {

        // const {statusCode, data} = await config.postRequest('https://api.pushe.co/v2/messaging/notifications/', {
        //     app_ids: ['com.restaurant_app'],
        //     filters: {pushe_id: [customer.androidSubscription.subscription]},
        //     data: {
        //         title: "عنوان پیام",
        //         content
        //     }
        // }, {authorization: 'TOKEN ' + config.PUSHE_TOKEN,})

    }

}


// NOTE: webSubscription and androidSubscription must be populated on customer document
async function sendToAdmin(subscription, object, flagParam) {

    // Chrome has historically used GCM API, rather than the web push protocol, to send push messages.
    // The web-push library is designed to use the web push protocol and sort of fallback to GCM - since Chrome is doing a proprietary / non-standard thing here.
    // The GCM API doesn't use the 'TTL' header that web push protocol uses,
    // instead GCM API uses 'time_to_live' option if set, which I don't believe the web-push library uses.
    // webpush TTL does not work on chrome neither on firefox
    // let result = await webpush.sendNotification(admin.subscription, payload, {TTL: 60}).catch(err => config.log(err))

    await webpush.sendNotification( subscription, JSON.stringify(getPushOptions(object, flagParam) ), {vapidDetails: multipleHandler.getVapidDetails(flagParam)}).catch(err => {

            if (err.statusCode === consts.GONE_CODE) {
                config.log('admin webpush subscription was GONE')
            }

        }
    )

}

function getPushOptions(obj, flagParam) {

    const host = multipleHandler.getHostByFlag(flagParam)

    return {
        title: (obj.title)? obj.title: host.title,
        body: obj.content,
        icon: multipleHandler.getNotificationIconPath(flagParam),
        image: obj.image,
        url: (obj.url)? obj.url: host.base,
        requireInteraction: (obj.requireInteraction)? true: obj.requireInteraction,
    }
}



module.exports = {sendToAll, sendNotif, sendToAdmin}
