const mongoose = require('mongoose')
const webpush = require("web-push")
const dConverter = require('../tools/dateConverter')

const consts = require('./consts')
const errHandler = require('./errHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const adminHandler = require('./adminHandler')
const foodHandler = require('./foodHandler')
const discountHandler = require('./discountHandler')
const notificationHandler = require('./notificationHandler')
const customerHandler = require('./customerHandler')
const staticsHandler = require('./staticsHandler')

const faker = require('../tools/faker')
const admins = require('../routes/admins')



async function getOrders(req, res, next) {

    const {skipCount, sortBy, state, date, searchText, limit} = req.body

    const response = await getOrdersList(state, sortBy, skipCount, date, searchText, limit, req.phone, multipleHandler.getFlag(req))

    res.json(response)
}

async function getOrdersList(state, sortBy, skipCount, date, searchText, limit, phone, flagParam) {

    const [Order, Admin] = multipleHandler.getModel([consts.ORDER, consts.ADMIN], flagParam)

    let query

    // sort
    if (sortBy === consts.HIGHEST_SUM) sortBy = 'price'
    else sortBy = 'created' // sorting by created in default

    // state
    if (state === consts.ALL) query = { $or:[ {'state': consts.DELIVERED}, {'state': consts.REJECTED} ]}
    else if (state === consts.ACTIVE_ORDERS) query = { $or:[ {'state': consts.COMMITTED}, {'state': consts.ACCEPTED}, {'state': consts.SENT}]}
    else if (state === consts.DELIVERED) query = { 'state': consts.DELIVERED }
    else if (state === consts.REJECTED) query = { 'state': consts.REJECTED }

    // date
    if (date) query = {...query, created: { $regex: '.*' + date.year+'-'+date.month+'-'+date.day + '.*' }}

    // searchText
    if (searchText !== undefined && searchText !== '') {

        const firstChar = searchText.charAt(0)

        if (firstChar === '0') query = {...query, phone: { $regex: '.*' + searchText + '.*' } }
        else if (config.isDigit(firstChar)) query = {...query, code: { $regex: '.*' + searchText + '.*' } }
    }

    // phone (in customer requests)
    if (phone)
        query = {...query, phone}

    // await new Promise((res, reject) => {
    //     setTimeout(() => {
    //         res()
    //     }, 3000)
    // })


    let result

    if (state !== consts.ACTIVE_ORDERS) // requesting finished orders
        result = await Order.find( query, { __v: 0 },{ sort: { [sortBy] : -1 } }).populate({path: 'owner', select: 'name phone',
            match : { // this shit just matches refs query itself not the main query result
                // name: { $regex: '.*' + 'حسین' + '.*' }
            }
        }).skip(skipCount).limit(limit)

    else // requesting active orders
        result = await Order.find( query, { __v: 0 },{ sort: { [sortBy] : -1 } }).populate('owner', 'name phone')


    return { orders: result}
}

async function getComments(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Order] = multipleHandler.getModel([consts.ORDER], flagParam)

    const {skipCount, limit} = req.body


    const result = await Order.find({ score: { $exists: true }},  { code: 1, score: 1 },{ sort: { 'score.created' : -1 } })
        .skip(skipCount).limit(limit).populate('owner', 'name')

    // config.log(result)

    res.json({comments: result})
}



async function newOrder(req, res, next) {

    let issue = false
    let { foods, _id } = req.body

    let response = {
        price: 0,
        cPrice: 0,
        discounts: 0,
        dPrice: 0, // discounted price
    }

    let {customer} = await checkFoodsAndAddressAndDistance(req, res, foods, _id, response)

    if (!customer) return // checking was not successful

    res.json(response)
}

async function submitOrder(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Order, Transaction] = multipleHandler.getModel([consts.ORDER, consts.TRANSACTION], flagParam)

    let issue = false
    let { foods, _id, method, useCredit, requestATM, code } = req.body

    let orderDetail = {
        price: 0,
        cPrice: 0,
        discounts: 0,
        dPrice: 0, // discounted price
    }

    let {customer, admin, address, distance} = await checkFoodsAndAddressAndDistance(req, res, foods, _id, orderDetail)

    if (!customer) return // checking was not successful

    if (customer.banned) return res.status(consts.BAD_REQ_CODE).send('ثبت سفارش برای شما مسدود شده، لطفا با پشتیبانی تماس بگیرید')

    let discountedPrice = orderDetail.dPrice
    let discounts = orderDetail.discounts


    let discountCode = null
    let inviterCode = null





    // چون امکان لغو شدن سفارش تا آخرین مرحله یعنی تحویل به مشتری هست
    // و نمیخوایم کاربر بتونه با سفارش های موازی از یه کد تخفیف چند بار استفاده کنه
    // همچنین نمیتونیم اعتبار کاربر تا آخرین مرحله تغییر ندیم به علت بالا
    // مجبوریم همه این کار ها رو بکنیم و در دستور لغو شدن سفارش در هر مرحله ای این کار ها رو برگردونیم
    if (code) {

        const isDiscountCode = config.includesLetter(code) // discountCode or customerCode

        if (isDiscountCode) {

            discountCode = await discountHandler.checkCodeValidity(code, orderDetail.dPrice, req, res)

            if (!discountCode) return

            discountCode = await discountHandler.canCustomerUseDiscount(discountCode, orderDetail.dPrice, req, res)

            if (!discountCode) return

            customer.usedDiscounts.push(code)



            discountedPrice = Math.round(orderDetail.dPrice/100*(100 - discountCode.discount))

        } else { // user is using invite discount


            let {discount, inviter} = await discountHandler.checkInviterCodeValidity(code, orderDetail.dPrice, req, res)

            if (!discount) return

            // inviter = await Customer.findOne({code})
            // if (!inviter) return res.status(consts.BAD_REQ_CODE).send('این کد تخفیف وجود ندارد')
            //
            // const notRejectedOrdersCount = await Order.estimatedDocumentCount({owner: customer._id, state: {$ne: consts.REJECTED}})
            // if (notRejectedOrdersCount !== 0) return res.status(consts.BAD_REQ_CODE).send('این کد برای اولین سفارش می باشد')
            //
            // if (admin.invitation.minPrice > orderDetail.dPrice) return res.status(consts.BAD_REQ_CODE)
            //         .send('این کد تخفیف برای سفارش های بالای ' + config.getThousandToman(admin.invitation.minPrice) + ' هزار تومن می باشد')


            inviterCode = inviter.code

            discountedPrice = Math.round(orderDetail.dPrice/100*(100 - discount))

            customer.inviter = inviter._id

        }
    }

    discounts = orderDetail.discounts + (orderDetail.dPrice - discountedPrice)


    let creditSpent = 0
    let payable = discountedPrice + orderDetail.cPrice

    if (useCredit === true)
        if (customer.credit > payable) {
            creditSpent = payable
            payable = 0
        }
        else {
            creditSpent = customer.credit
            payable = payable - customer.credit
        }





    let order = await new Order({
        code:  faker.getRandomCode(6),
        owner: customer._id,
        phone: customer.phone,
        state: consts.COMMITTED,
        discountCode: (discountCode)? code: undefined,
        inviterCode: (inviterCode)? code: undefined,
        foods: foods,
        address: address.address,
        location: address.location,
        telePhone: address.telePhone,
        distance: distance,
        price: orderDetail.price,
        cPrice: orderDetail.cPrice,
        discounts: discounts,
        credit: creditSpent,
        payable,
        wantAtm: (payable === 0)? false: requestATM
    })


    customer.credit = customer.credit - creditSpent
    customer.orders.push(order)


    // if payable was 0 commit method as CASH payment order
    if (method === consts.ONLINE && order.payable !== 0) {

        order.state = consts.TO_BE_PAID
        order.method = consts.ONLINE


        // TODO: remove this
        return res.status(consts.BAD_REQ_CODE).send('درگاه پرداخت فعال نیست، از پرداخت نقدی استفاده کنید')




        const data = await config.postToPhp(config.AQAYE_PARDAKHT_CREATE_TRANSACTION, {
            amount: order.payable,
            pin: config.AQAYE_PARDAKHT_PIN,
            // pin: 'F69B649E9707E1955D0A',
            callback: config.baseAddress + consts.ROUTE_PAYMENT_CALLBACK
        })

        if (!data) return res.status(consts.BAD_REQ_CODE).send('مشکل در ارتباط با درگاه پرداخت')


        config.log('\n')
        config.log('AP response: ' + data)


        // if response length is less that 3, it is an error code
        if (data.length < 3) return res.sendStatus(consts.INT_ERR_CODE)


        await new Transaction({
            transid: data,
            amount: order.payable,
            owner: customer._id
        }).save(err => {
            if(err) config.log(err)
        })

        order.transid = data

        res.json({url: config.AQAYE_PARDAKHT_PAY_LINK+data })


    } else if (method === consts.CASH || order.payable === 0) {

        order.state = consts.COMMITTED
        order.method = consts.CASH



        // let response = await customer.populate('orders', '-__v -owner').execPopulate()
        // config.log(response)

        // in case user credit was changed
        res.json(customer)


        if (admin.subscription)
            notificationHandler.sendToAdmin(admin.subscription, {
                title: "سفارش جدید !!!",
                content: config.getFormattedFoods(foods),
                url: multipleHandler.getHost(req).admin + '/active-orders'
            }, flagParam)

    }




    await order.save().catch(err => config.log(err))

    await customer.save().catch(err => config.log(err))

}


async function checkFoodsAndAddressAndDistance(req, res, foods, _id, response) {

    const flagParam = multipleHandler.getFlag(req)
    const [Customer, Admin] = multipleHandler.getModel([consts.CUSTOMER, consts.ADMIN], flagParam)

    // ensure that all foods exist
    const menu = await foodHandler.getMenu(req)

    for (let i = 0; i < foods.length; i++) {
        for (let j = 0; j < menu.length; j++) {

            if (foods[i].name === menu[j].name) {

                response.price += menu[j].price * foods[i].count
                response.dPrice += menu[j].dPrice * foods[i].count
                break
            }

            if (j === menu.length - 1)  // if it is the last loop and loop did not break --> we didn't found one food
                return res.status(consts.BAD_REQ_CODE).send(consts.FOOD_LIST_INCOMPATIBILITY)
        }
    }

    response.discounts = response.price - response.dPrice


    // we set req.phone in custmerAuth function
    const customer = await Customer.findOne({phone: req.phone})

    let cAddress

    // calculate distance
    customer.addresses.forEach(address => {
        // comparing mongoID
        if (address._id == _id)  cAddress = address
    })


    const admin = await Admin.findOne()


    // TODO: uncomment, commented for payment gateway test
    // check minimum order price
    // if (admin.leastPrice > response.dPrice) return res.status(consts.BAD_REQ_CODE).send('حداقل مبلغ سفارش ' + admin.leastPrice + ' تومان می باشد')

    // check acceptOrder
    if (admin.acceptOrder === consts.FALSE) return res.status(consts.BAD_REQ_CODE).send(consts.ERR_NOT_IN_ACCEPT_ORDER_TIME)
    if (admin.acceptOrder === consts.OFF) return res.status(consts.BAD_REQ_CODE).send(consts.ERR_ORDER_TEMPORARY_OFF)


    let distance


    // Neshan api makes request lazy, uncomment if u want to use it
    // const {statusCode, data} = await config.getRequest(
    //     `https://api.neshan.org/v1/distance-matrix?origins=${admin.location.lat},${admin.location.lng}&destinations=${cAddress.location.lat},${cAddress.location.lng}`,
    //     {headers: {'Api-Key': config.NESHAN_API_KEY}})
    //
    // // config.log(data)
    //
    // for (let i = 0; i < data.rows.length; i++) {
    //
    //     for (let j = 0; j < data.rows[i].elements.length; j++) {
    //
    //         config.log(data.rows[i].elements[j])
    //         distance = data.rows[i].elements[j].distance.value
    //     }
    // }

    distance = Math.round(config.distanceInKmBetweenEarthCoordinates(admin.location.lat, admin.location.lng, cAddress.location.lat, cAddress.location.lng)*1000)

    config.log('distance: ' + distance)


    // check maximum distance
    if (distance > admin.maxDistance) return res.status(consts.BAD_REQ_CODE).send('سرویس دهی در آدرس مورد نظر صورت نمیگیرد')


    if (distance < 1000) response.cPrice = admin.courier['0-1']
    else if (distance < 2000) response.cPrice = admin.courier['1-2']
    else if (distance < 3000) response.cPrice = admin.courier['2-3']
    else response.cPrice = admin.courier['3-u']


    return {customer, admin, address: cAddress, distance}
}

/*
* change order state by admin
* */
async function handleOrder(req, res, next) {

    const flagParam = multipleHandler.getFlag(req)
    const [Order, Customer, Admin] = multipleHandler.getModel([consts.ORDER, consts.CUSTOMER, consts.ADMIN], flagParam)

    let issue = false

    const {state, orderId, ownerPhone} = req.body

    // NOTE: findOneAndUpdate returns object before update by default, but you can set new: true option to get new document
    let order = await Order.findOneAndUpdate({_id: orderId},  {  state }, { new: true } )



    // sending back active orders to refresh list
    let response = await getOrdersList(consts.ACTIVE_ORDERS, consts.LATEST, 0,
        undefined, undefined, undefined, undefined, flagParam)

    res.json(response)


    let customer = await Customer.findOne({phone: ownerPhone}).populate('androidSubscription').populate('webSubscription')

    let content
    if (state === consts.ACCEPTED) content = `سفارش شما وارد مرحله آماده سازی شده است`
    else if (state === consts.SENT) content = `سفارش شما در حال ارسال توسط پیک است`


    if (state === consts.DELIVERED) { // برای اینکه دعوت کننده نتونه در حین سفارش دعوت شده اعتبار بگیره و استفاده کنه بعد اتمام سفارش بهش میدیم

        // if customer used invite code in order
        if (order.inviterCode) {

            let inviter = await Customer.findOne({_id: customer.inviter}).populate('androidSubscription').populate('webSubscription')
            const admin = await Admin.findOne()

            inviter.inviteds.push(customer._id)
            inviter.invitedsCount++
            inviter.credit += admin.invitation.credit

            await inviter.save().catch(err => {
                if (err) config.log(err)
            })

            notificationHandler.sendNotif(inviter, {content: config.getFormattedPrice(admin.invitation.credit) + ' تومان به اعتبار شما افزوده شد'}, flagParam)

        }

        customer.ordersCount++

        await customer.save().catch(err => {
            if (err) config.log(err)
        })


        // update statics
        staticsHandler.incrementOrder(order.credit+order.payable, flagParam)



    } else if (state === consts.REJECTED) {

        content = `سفارش شما توسط رستوران لغو شده است`

        await handleOrderCancel(order, null, flagParam)

    }




    if (state === consts.DELIVERED) {

        setTimeout(() => {
            notificationHandler.sendNotif(
                customer,{ content: 'به سفارش خودتون امتیاز بدید و نظرتون رو بگید',
                    url: multipleHandler.getHost(req).base + '/review/' + order._id
            }, flagParam)

        }, 1*60*1000)

    } else
        await notificationHandler.sendNotif(customer, {content}, flagParam)


}

async function paymentCallback(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Transaction, Order] = multipleHandler.getModel([consts.TRANSACTION, consts.ORDER], flagParam)

    config.log('Order POST Callback')

    const { transid } = req.body

    if (!transid) return res.sendStatus(consts.BAD_REQ_CODE)

    let order = await Order.findOne({transid})
    let transaction = await Transaction.findOne({transid})

    if (!order || !transaction) return res.sendStatus(consts.NOT_FOUND_CODE)


    const data = await config.postToPhp(config.AQAYE_PARDAKHT_VERIFY_TRANSACTION, {
        pin: config.AQAYE_PARDAKHT_PIN,
        amount: order.payable,
        transid: order.transid
    })

    config.log(data)

    if (data == 1) { // transaction was successful

        order.state = consts.COMMITTED
        transaction.state = consts.PAYED

        // چون توی پنل ادمین میخوایم به ترتیب جدیدترین سفارش لیست فعال رو نشون بدیم، ممکنه در حین پرداخت سفارش های نقدی جدیدتر ثبت بشه
        order.created = dConverter.getLiveDate()

        await order.save().catch(err => config.log(err))

        await transaction.save().catch(err => config.log(err))


    } else if (data == 0) {  // transaction was NOT successful


        // order = await order.populate('owner', '-__v').execPopulate()

        await handleOrderCancel(order, transid, flagParam)

    }



    res.writeHead(302, {'Location': config.baseAddress + consts.ROUTE_PAYMENT_CALLBACK})
    res.end()
}


async function handleOrderCancel(order, transid, flagParam) {

    const [Customer, Transaction, Order] = multipleHandler.getModel([consts.CUSTOMER, consts.TRANSACTION, consts.ORDER], flagParam)

    let customer = await Customer.findOne({_id: order.owner})

    // if credit was spent
    if (order.credit !== 0) customer.credit += order.credit

    // if online payment was done, add all payment to customer's credit
    if (order.method === consts.ONLINE && order.state !== consts.TO_BE_PAID) customer.credit += order.payable


    if (order.discountCode) customer.usedDiscounts.splice(customer.usedDiscounts.indexOf(order.discountCode), 1)
    else if (order.inviterCode) customer.inviter = undefined


    await customer.save().catch(err => config.log(err))


    // if transid is sent , so we are handling a transaction fail and must also delete order and transaction
    if (transid) {

        await Order.deleteOne({transid}).catch(err => config.log(err))
        await Transaction.deleteOne({transid}).catch(err =>  config.log(err))
    }
}

async function scoreOrder(req, res) {

    const flagParam = multipleHandler.getFlag(req)
    const [Order] = multipleHandler.getModel([consts.ORDER], flagParam)

    const {_id, foodScore, courierScore, appScore, comment} = req.body

    // setting phone in query to be sure that authorized user is reviewing his own order
    const order = await Order.findOne({_id, phone: req.phone})

    if (!order) return res.status(consts.NOT_FOUND_CODE).send('سفارش پیدا نشد')

    if (order.score.food) return res.status(consts.BAD_REQ_CODE).send('این سفارش قبلا امتیاز دهی شده است')



    const now = dConverter.getLiveDate()

    const date1 = new Date(
        order.created.substr(0, 4),
        order.created.substr(5, 2),
        order.created.substr(8, 2),
        order.created.substr(11, 2),
        order.created.substr(14, 2))

    const date2 = new Date(
        now.substr(0, 4),
        now.substr(5, 2),
        now.substr(8, 2),
        now.substr(11, 2),
        now.substr(14, 2)
    )


    let diff = (date2 - date1)/(1000*3600)

    if (diff > 72)
        return res.status(consts.BAD_REQ_CODE).send('مهلت امتیاز دهی به این سفارش پایان یافته')


    // config.log('diff : ' + diff)


    await Order.updateOne({_id, phone: req.phone},
        { score: {food: foodScore, courier: courierScore, app: appScore, comment, created: now} })



    res.sendStatus(consts.SUCCESS_CODE)
}







module.exports = { getOrders, newOrder, submitOrder, getOrdersList, handleOrder, paymentCallback, handleOrderCancel, scoreOrder, getComments}
