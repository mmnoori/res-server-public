const consts = require('./consts')
const errHandler = require('./errHandler')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const validator = require('../tools/validator')
const dConverter = require('../tools/dateConverter')
const faker = require('../tools/faker')

const admins = require('../routes/admins')
const adminHandler = require('./adminHandler')
const staticsHandler = require('./staticsHandler')




async function getReports(req, res, next) {

    const {skipCount, limit} = req.body

    // req.code from jwt auth
    const response = await getReportsList(skipCount, limit, multipleHandler.getFlag(req))

    res.json(response)
}

async function getReportsList(skipCount, limit, flagParam) {

    const [Report] = multipleHandler.getModel([consts.REPORT], flagParam)

    const result = await Report.find( {}, { __v:0, }, { sort: {created: -1} } ).skip(skipCount).limit(limit)

    result.forEach(report => report.created.substr(0,10) )

    return { reports: result}
}










module.exports = {getReports, getReportsList, }
