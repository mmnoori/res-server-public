const { detect } = require('detect-browser')

const consts = require('./consts')
const config = require('../config/config')
const multipleHandler = require('../config/multipleHandler')
const dConverter = require('../tools/dateConverter')


function incrementView(req, res, next) {

    const [Statics] = multipleHandler.getModel([ consts.STATICS], multipleHandler.getFlag(req))

    // if session viewed was saved
    // if (req.session.viewed) return

    // NOTE: connect mongo saves sessions asyncly, so even after end of response it would take a while for session to be saved
    // we have of course parallel saving issue or accessing an under modify session if user send consecutive requests like an animal
    // req.session.viewed = true

    const key = multipleHandler.getAssetsKey(req)


    // if was already viewed
    if (req.cookies[key] && req.cookies[key].vid) return next()

    // NOTE: request must be sent from client-side to set cookie not getInitialProps
    // viewed true
    res.cookie(key, {vid: true}, { httpOnly: true, maxAge: 24 * 3600 * 1000 })


    const browser = detect(req.headers['user-agent'])

    let object = {}

    if (browser) object = { [`browser.${browser.name}`] : 1, [`OS.${[browser.os]}`] : 1 }
    else object = { notDetected : 1 }

    const {year, month, day} = dConverter.getJalaliDate()

    // saving asyncly
    Statics.updateOne({}, {year,
        $inc : {[`metrics.${month}.${day}.views`]: 1, [`metrics.${month}.views`]: 1, views: 1, ...object}
    }, {upsert: true,}).catch(err => config.log(err))

    // session.save() is automatically called at the end of the HTTP response if the session data has been altered

    next()
}

function incrementCustomer(flagParam) {

    const [Statics] = multipleHandler.getModel([ consts.STATICS], flagParam)

    const {year, month, day} = dConverter.getJalaliDate()

    Statics.updateOne({}, {year,
        $inc : {[`metrics.${month}.${day}.customers`]: 1, [`metrics.${month}.customers`]: 1, customers: 1,}
    }, {upsert: true,}).catch(err => config.log(err))

}

// must handle sale here either
function incrementOrder(orderPrice, flagParam) {

    const [Statics] = multipleHandler.getModel([ consts.STATICS], flagParam)

    const {year, month, day} = dConverter.getJalaliDate()

    Statics.updateOne({}, {year,
        $inc : {
            [`metrics.${month}.${day}.orders`]: 1, [`metrics.${month}.orders`]: 1, orders: 1,
            [`metrics.${month}.${day}.sale`]: orderPrice, [`metrics.${month}.sale`]: orderPrice, sale: orderPrice,
        }
    }, {upsert: true,}).catch(err => config.log(err))

}










module.exports = {incrementView, incrementCustomer, incrementOrder }
